package com.maoti.lib.db;

public interface SPFKey {
    String TOKEN="TOKEN";
    String IsSingIN = "ISSINGIN";
    String WhiteAndheaven = "WhiteAndheaven";
    String USER_ID="user_id";
    String USER_TYPE = "user_type";
    String EMOJJ = "keys_emojj";
    String LOCAL_KEY_WORD="LocalKeyword";
    String SHARE_IS_FIRST = "isFirst";  //判断程序是否是第一次运行
    String SHOW_AGREEMENT = "SHOW_AGREEMENT";  //是否显示用户协议
    String LoginBean = "LoginBean";

    String push_news = "push_news";
    String push_comments = "push_comments";
    String push_all = "push_all";
    String http_url="http_url";//保存http请求地址
    String ws_url="ws_url";//保存websokey地址
    String FAN_CIR_HASNEWS = "fan_cir_hasnews";//球迷圈是否有新说说
    String FAN_CIR_NEWEST_USER = "fan_cir_newest_user";//球迷圈最新说说的用户
    String FAN_CIR_NEW_DRAFIT = "fan_cir_new_drafit";//球迷圈的草稿(只保存最近一条)
    String FAN_CIR_NEW_DRAFIT_IMGS = "fan_cir_new_drafit_imgs";//球迷圈的草稿图片
    String POST_DRAFIT = "post_drafit";//帖子草稿
    String UNREAD_MSG_NUM = "unread_msg_num";//未读消息数

    String HW_TOKEN="HW_TOKEN";//华为token保存
    String FOLLOW_UNREAD = "follow_unread";//关注是否有未读消息
    String HOME_FOLLOW_UNREAD = "home_follow_unread";//home关注是否有未读消息
    String IS_FIRST_IN_CIR = "is_first_in_cir";//是否是打开猫体圈
    String FABULOUS = "fabulous";;//用来标记开发者模式
}
