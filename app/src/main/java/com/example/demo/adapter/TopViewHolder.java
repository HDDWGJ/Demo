package com.example.demo.adapter;

import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.AbsListView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.demo.MainActivity;
import com.example.demo.R;
import com.example.demo.model.DataBean;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by hbh on 2017/4/20.
 * 父布局ViewHolder
 */

public class TopViewHolder extends BaseViewHolder {

    private Context mContext;
    private View view;
    private TextView mShrinkText;
    private String mCollect = "收起";
    private String mOpen = "展开";
    private RecyclerView mRecyclerView;
    private TopViewAdapter mAdapter;

    public TopViewHolder(Context context, View itemView) {
        super(itemView);
        this.mContext = context;
        this.view = itemView;
    }

    public void bindView(final DataBean dataBean, final int pos,final ItemClickListener listener){
        mShrinkText = view.findViewById(R.id.shrink);
        setRecyclerView();

    }
    private void setRecyclerView(){
        mRecyclerView = view.findViewById(R.id.recycle_view);
       final List list = new ArrayList();
        for (int i=0;i<10;i++){
            list.add(i);
        }
        mRecyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        mAdapter = new TopViewAdapter(list, mContext);
        //纵向线性布局
        GridLayoutManager layoutManager = new GridLayoutManager(mContext,5);
        mRecyclerView.setLayoutManager(layoutManager);


        mShrinkText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mShrinkText.getText().toString().equals(mOpen)){
                    for (int i=0;i<5;i++){
                        list.add(i);
                    }
                    mAdapter.notifyDataSetChanged();
                    mShrinkText.setText(mCollect);
                }else {
                    for (int i=0;i<5;i++){
                        list.remove(i);
                    }
                    mAdapter.notifyDataSetChanged();
                    mShrinkText.setText(mOpen);
                }
            }
        });
        mRecyclerView.setAdapter(mAdapter);
    }



}
