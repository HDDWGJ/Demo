package com.example.demo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.demo.R;

import java.util.List;

public class TopViewAdapter extends RecyclerView.Adapter<TopViewAdapter.ViewHolde> {
    private Context context;
    private int chosePosition;
    private ListenerBack listenerBack;
    private List mList;

    public TopViewAdapter(List list,Context context) {
        this.context = context;

        this.mList = list;
    }


    @NonNull
    @Override
    public ViewHolde onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view =  LayoutInflater.from(context).inflate(R.layout.top_item, parent, false);

        return new ViewHolde(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolde holder, int position) {

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public interface ListenerBack{
        void backPosition(int position);
    }

    public static class ViewHolde extends RecyclerView.ViewHolder {

        TextView txt;
        ImageView img;
        public ViewHolde(View itemView) {
            super(itemView);
            txt = itemView.findViewById(R.id.text);
            img = itemView.findViewById(R.id.image);
        }
    }


}
