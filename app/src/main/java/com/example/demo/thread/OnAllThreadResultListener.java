package com.example.demo.thread;

/**
 * Created by Android Studio.
 * User: wsl
 * Date: 2020/12/15
 * Time: 3:40 PM
 * Description: 监听线程回调接口：
 */
public interface OnAllThreadResultListener {
    void onSuccess();//所有线程执行完毕
    void onFailed();//所有线程执行出现问题
}
