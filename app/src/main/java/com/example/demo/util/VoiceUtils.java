package com.example.demo.util;

import android.content.Context;
import android.media.MediaPlayer;
import android.text.TextUtils;


import com.example.demo.R;

import java.io.IOException;

/**
 * author : toby
 * e-mail : 13128826083@163.com
 * time : 2018/3/6
 * desc :
 */

public class VoiceUtils {

    private static volatile VoiceUtils singleton = null;
    public boolean playing;

    private MediaPlayer mediaPlayer = null;
    private Context mContext;

    public VoiceUtils(Context context) {
        this.mContext = context.getApplicationContext();
    }

    public static VoiceUtils with(Context context) {
        if (singleton == null) {
            synchronized (VoiceUtils.class) {
                if (singleton == null) {
                    singleton = new VoiceUtils(context);
                }
            }
        }
        return singleton;
    }

    public boolean isPlaying() {
        return playing;
    }

    public void setPlaying(boolean playing) {
        this.playing = playing;
    }

    public void play(String soundStr, String flag) {
        if (Utils.numberAndBigDigital(soundStr)) {
            String str = null;
            if (TextUtils.isEmpty(flag)) {
                str = soundStr;
            } else if (flag.equals("in")) {
                str = "入" + soundStr;
            } else if (flag.equals("out")) {
                str = soundStr + "出";
            } else {
                str = soundStr;
            }
            playSoundList(1, str, str.length());
        } else {
            // 可能包含0~9或A~Z之外的字符、则不播报语音
        }
    }

    public void playSoundList(final int soundIndex, final String soundStr, final int soundCount) {
        singleton.setPlaying(true);
//        if (mediaPlayer == null) {
//            mediaPlayer = null;
//        }

//        LogUtils.d("----加载音频---->" + soundIndex);
        mediaPlayer = createSound(soundIndex, soundStr);

//        LogUtils.d("-----加载音频成功----->");

        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                mediaPlayer.release();
                int newSoundIndex = soundIndex;
                if (soundIndex < soundCount) {
                    newSoundIndex = newSoundIndex + 1;
                    playSoundList(newSoundIndex, soundStr, soundCount);
                } else {
                    singleton.setPlaying(false);
                }
            }
        });

        try {
            //在播放音频资源之前，必须调用Prepare方法完成些准备工作
            mediaPlayer.prepare();
            //开始播放音频
            mediaPlayer.start();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public MediaPlayer createSound(int soundIndex, String soundStr) {
        MediaPlayer mp = null;
        String soundChar = soundStr.substring(soundIndex - 1, soundIndex);
        switch (soundChar) {
            case "0":
                mp = MediaPlayer.create(mContext, R.raw.number0);
                break;
            case "1":
                mp = MediaPlayer.create(mContext, R.raw.number1);
                break;
            case "2":
                mp = MediaPlayer.create(mContext, R.raw.number2);
                break;
            case "3":
                mp = MediaPlayer.create(mContext, R.raw.number3);
                break;
            case "4":
                mp = MediaPlayer.create(mContext, R.raw.number4);
                break;
            case "5":
                mp = MediaPlayer.create(mContext, R.raw.number5);
                break;
            case "6":
                mp = MediaPlayer.create(mContext, R.raw.number6);
                break;
            case "7":
                mp = MediaPlayer.create(mContext, R.raw.number7);
                break;
            case "8":
                mp = MediaPlayer.create(mContext, R.raw.number8);
                break;
            case "9":
                mp = MediaPlayer.create(mContext, R.raw.number9);
                break;
            case "A":
                mp = MediaPlayer.create(mContext, R.raw.number_a);
                break;
            case "B":
                mp = MediaPlayer.create(mContext, R.raw.number_b);
                break;
            case "C":
                mp = MediaPlayer.create(mContext, R.raw.number_c);
                break;
            case "D":
                mp = MediaPlayer.create(mContext, R.raw.number_d);
                break;
            case "E":
                mp = MediaPlayer.create(mContext, R.raw.number_e);
                break;
            case "F":
                mp = MediaPlayer.create(mContext, R.raw.number_f);
                break;
            case "G":
                mp = MediaPlayer.create(mContext, R.raw.number_g);
                break;
            case "H":
                mp = MediaPlayer.create(mContext, R.raw.number_h);
                break;
            case "I":
                mp = MediaPlayer.create(mContext, R.raw.number_i);
                break;
            case "J":
                mp = MediaPlayer.create(mContext, R.raw.number_j);
                break;
            case "K":
                mp = MediaPlayer.create(mContext, R.raw.number_k);
                break;
            case "L":
                mp = MediaPlayer.create(mContext, R.raw.number_l);
                break;
            case "M":
                mp = MediaPlayer.create(mContext, R.raw.number_m);
                break;
            case "N":
                mp = MediaPlayer.create(mContext, R.raw.number_n);
                break;
            case "O":
                mp = MediaPlayer.create(mContext, R.raw.number_o);
                break;
            case "P":
                mp = MediaPlayer.create(mContext, R.raw.number_p);
                break;
            case "Q":
                mp = MediaPlayer.create(mContext, R.raw.number_q);
                break;
            case "R":
                mp = MediaPlayer.create(mContext, R.raw.number_r);
                break;
            case "S":
                mp = MediaPlayer.create(mContext, R.raw.number_ss);
                break;
            case "T":
                mp = MediaPlayer.create(mContext, R.raw.number_t);
                break;
            case "U":
                mp = MediaPlayer.create(mContext, R.raw.number_u);
                break;
            case "V":
                mp = MediaPlayer.create(mContext, R.raw.number_v);
                break;
            case "W":
                mp = MediaPlayer.create(mContext, R.raw.number_w);
                break;
            case "X":
                mp = MediaPlayer.create(mContext, R.raw.number_x);
                break;
            case "Y":
                mp = MediaPlayer.create(mContext, R.raw.number_y);
                break;
            case "Z":
                mp = MediaPlayer.create(mContext, R.raw.number_z);
                break;
            case "入":
                mp = MediaPlayer.create(mContext, R.raw.upbox);
                break;
            case "出":
                mp = MediaPlayer.create(mContext, R.raw.fetchsuccess);
                break;
        }
        mp.stop();
        return mp;
    }

}
