package com.example.demo.util;

import android.app.Activity;
import android.app.AppOpsManager;
import android.app.PendingIntent;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.telephony.SmsManager;
import android.text.InputType;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;



import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Formatter;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by lpx on 2016/1/5.
 */
public class Utils {

    /**
     * 获取版本号名称
     *
     * @return 当前应用的版本号名称
     */
    public static String getAppVersionName(Context context) {
        try {
            PackageManager manager = context.getPackageManager();
            PackageInfo packageInfo = manager.getPackageInfo(context.getPackageName(), 0);
            String version = packageInfo.versionName;
            return version;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return "";
        }
    }

    /**
     * 获取版本号
     *
     * @return 当前应用的版本号
     */
    public static int getAppVersionCode(Context context) {
        try {
            PackageManager manager = context.getPackageManager();
            PackageInfo packageInfo = manager.getPackageInfo(context.getPackageName(), 0);
            int versionCode = packageInfo.versionCode;
            return versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * 开启软键盘
     */
    public static void openSoftKeyboard(EditText et) {
        InputMethodManager inputManager = (InputMethodManager) et.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
//        inputManager.showSoftInput(et, 0);
        inputManager.showSoftInput(et, InputMethodManager.SHOW_FORCED);
    }

    /**
     * 关闭软键盘
     */
    public static void closeSoftKeyboard(Context context) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        if (inputMethodManager != null && ((Activity) context).getCurrentFocus() != null) {
            inputMethodManager.hideSoftInputFromWindow(((Activity) context).getCurrentFocus()
                    .getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    /**
     * 打开或关闭软键盘
     *
     * @param context
     */
    public static void toggleSoftKeyboard(Context context) {
        if (context != null) {
            InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (imm.isActive()) {
                imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, InputMethodManager.HIDE_NOT_ALWAYS);
            } else {
                imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, InputMethodManager.SHOW_FORCED);
            }
        }
    }

    /**
     * 获取格式化时间
     *
     * @param date
     * @return
     */
    public static String getFormatDate(Date date) {
        String str = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        str = sdf.format(date);
        return str;
    }

    public static String getFormatDate2(Date date) {
        String str = null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        str = sdf.format(date);
        return str;
    }

    /**
     * 获取格式化时间
     *
     * @param date
     * @return
     */
    public static String getFormatDateWithTime(Date date) {
        String str;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        str = sdf.format(date);
        return str;
    }

    /**
     * 获取格式化时间，精确到秒数
     *
     * @param date
     * @return
     */
    public static String getFormatDateWithSeconds(Date date) {
        String str;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        str = sdf.format(date);
        return str;
    }

    /**
     * 获取格式化时间，精确到分钟数
     *
     * @param date
     * @return
     */
    public static String getFormatDateWithMinutes(Date date) {
        String str;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        str = sdf.format(date);
        return str;
    }

    public static String getFormatDateWithHourAndSeconds(Date date) {
        String str;
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        str = sdf.format(date);
        return str;
    }

    /**
     * 获取格式化时间，用于显示通知的时间
     *
     * @param date
     * @return
     */
    public static String getFormatDateForNotication(Date date) {
        String str;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");
        str = sdf.format(date);
        return str;
    }

    /**
     * 获取月日
     *
     * @param date
     * @return
     */
    public static String getFormateDateOnly(Date date) {
        String str;
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd");
        str = sdf.format(date);
        return str;
    }

    public static String getFormateDateCNOnly(Date date) {
        String str;
        SimpleDateFormat sdf = new SimpleDateFormat("MM月dd日");
        str = sdf.format(date);
        return str;
    }

    public static String getFormateDateWithMinuteCNOnly(Date date) {
        String str;
        SimpleDateFormat sdf = new SimpleDateFormat("MM月dd日 HH:mm");
        str = sdf.format(date);
        return str;
    }

    /**
     * 获取小时和分钟
     *
     * @param date
     * @return
     */
    public static String getFormateTimeOnly(Date date) {
        String str;
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        str = sdf.format(date);
        return str;
    }

    /**
     * 获取订单的时间显示
     *
     * @param date
     * @return
     */
    public static String getFormateDateOfOrder(Date date) {
        String str;
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd HH:mm");
        str = sdf.format(date);
        return str;
    }

    /**
     * 微书的创建时间
     *
     * @param date
     * @return
     */
    public static String getFormateDateOfWeBook(Date date) {
        String str;
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd HH:mm");
        str = sdf.format(date);
        return str;
    }

    /**
     * 校验用户手机号码格式
     *
     * @param phone
     * @return
     */
    public static boolean isPhoneNumber(String phone) {
        boolean flag = false;
        String regExp = "^((13[0-9])|(15[^4,\\D])|(17[0,6,7,8])|(18[0-9])|(14[5,7]))\\d{8}$";
        Pattern pattern = Pattern.compile(regExp);
        Matcher matcher = pattern.matcher(phone);
        flag = matcher.matches();
        return flag;
    }

    public static boolean isPhoneNumber2(String phone) {
        boolean flag = false;
        if (TextUtils.isEmpty(phone)) {
            flag = false;
        } else {
            if (phone.length() == 11 && phone.startsWith("1") && isAllDigital(phone)) {
                flag = true;
            }
        }
        return flag;
    }

    /**
     * 校验用户登录密码(6-18位数字或字母组合)
     *
     * @param pwd
     * @return
     */
    public static boolean isRightPwd(String pwd) {
        boolean flag = false;
        String regExp = "^[0-9A-Za-z]{6,18}$";
        Pattern pattern = Pattern.compile(regExp);
        Matcher matcher = pattern.matcher(pwd);
        flag = matcher.matches();
        return flag;
    }

    /**
     * 校验韵达快递单号
     *
     * @param code
     * @return
     */
    public static boolean isRightOrderCode(String code) {
        boolean flag;
        String regExp = "^[0-9]{13}$";
        Pattern pattern = Pattern.compile(regExp);
        Matcher matcher = pattern.matcher(code);
        flag = matcher.matches();
        return flag;
    }

    /**
     * 字符串纯数字校验
     *
     * @param code
     * @return
     */
    public static boolean isAllDigital(String code) {
        for (int i = 0; i < code.length(); i++) {
            if (!Character.isDigit(code.charAt(i))) {
                return false;
            }
        }
        return true;
    }

    public static void toast(Context context, String text) {
        Toast t = Toast.makeText(context, text, Toast.LENGTH_LONG);
        t.show();
    }

    /**
     * 字符串去空格
     *
     * @param str
     * @return
     */
    public static String deleteSpace(String str) {
        if (TextUtils.isEmpty(str)) {
            return "";
        } else {
            return str.replaceAll("\\s*", "");
        }
    }

    /**
     * 将月份数字转换为文字月份
     *
     * @param month
     * @return
     */
    public static String getMonthCN(String month) {
        String monthCN = "";
        if (month.equals("01") || month.equals("1")) {
            monthCN = "一月";
        } else if (month.equals("02") || month.equals("2")) {
            monthCN = "二月";
        } else if (month.equals("03") || month.equals("3")) {
            monthCN = "三月";
        } else if (month.equals("04") || month.equals("4")) {
            monthCN = "四月";
        } else if (month.equals("05") || month.equals("5")) {
            monthCN = "五月";
        } else if (month.equals("06") || month.equals("6")) {
            monthCN = "六月";
        } else if (month.equals("07") || month.equals("7")) {
            monthCN = "七月";
        } else if (month.equals("08") || month.equals("8")) {
            monthCN = "八月";
        } else if (month.equals("09") || month.equals("9")) {
            monthCN = "九月";
        } else if (month.equals("10")) {
            monthCN = "十月";
        } else if (month.equals("11")) {
            monthCN = "十一月";
        } else if (month.equals("12")) {
            monthCN = "十二月";
        } else {
            monthCN = month;
        }
        return monthCN;
    }

    /**
     * 格式化钱数
     *
     * @param money
     * @return
     */
    public static String getFormatPrice(double money) {
        String price;
        Formatter formatter = new Formatter();
        price = formatter.format("%.2f", money).toString();
        return price;
    }

    /**
     * 格式化钱数
     *
     * @param money
     * @return
     */
    public static String getFormatPrice2(double money) {
        String price;
        Formatter formatter = new Formatter();
        price = formatter.format("%.1f", money).toString();
        return price;
    }

    /**
     * 格式化钱数
     *
     * @param marks
     * @return
     */
    public static String getFormatMarks(float marks) {
        String price;
        Formatter formatter = new Formatter();
        price = formatter.format("%.1f", marks).toString();
        return price;
    }

    /**
     * 隐藏系统键盘
     *
     * @param editText
     */
    public static void hideSystemSofeKeyboard(Context context, EditText editText) {
        int sdkInt = Build.VERSION.SDK_INT;
        if (sdkInt >= 11) {
            try {
                Class<EditText> cls = EditText.class;
                Method setShowSoftInputOnFocus;
                setShowSoftInputOnFocus = cls.getMethod("setShowSoftInputOnFocus", boolean.class);
                setShowSoftInputOnFocus.setAccessible(true);
                setShowSoftInputOnFocus.invoke(editText, false);
            } catch (SecurityException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            editText.setInputType(InputType.TYPE_NULL);
        }
        // 如果软键盘已经显示，则隐藏
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
//        imm.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
    }

    /**
     * 是否是闰年
     *
     * @param year
     */
    public static boolean isLeapYear(int year) {
        boolean result = false;
        if ((year % 4 == 0 && year % 100 != 0) || (year % 400) == 0) {
            result = true;
        }
        return result;
    }

    /**
     * 格式化处理运单号
     *
     * @param expressId
     * @return
     */
    public static String getFormatedExpressId(String expressId) {
        String result = "";
        if (TextUtils.isEmpty(expressId)) {
            result = "运单号不存在";
        } else {
            int size = expressId.length();// 运单号的位数
            int groupSize = size / 3;// 组数
            int groupMore = size % 3;// 最后一组的长度
            int start = 0;
            int end = 3;
            if (groupSize > 0) {
                for (int i = 0; i < groupSize; i++) {
                    if (i == groupSize - 1) {
                        result = result + expressId.substring(start, end);
                    } else {
                        result = result + expressId.substring(start, end) + " ";
                    }
                    start = end;
                    end = start + 3;
                }
                if (groupMore != 0) {
                    // 分了groupSize组后还有余数groupMore
                    result = result + " " + expressId.substring(start, size);
                }
            } else {
                // expressId少于3位，不足一组，不做处理
                result = expressId;// 直接赋值
            }
        }
        return result;
    }

    public static void sendMessage(String phoneNumber, String message, PendingIntent sendPendingIntent) {
        // 获取短信管理器
        SmsManager smsManager = SmsManager.getDefault();
        // 拆分短信内容(手机短信长度限制)
        List<String> divideContents = smsManager.divideMessage(message);
        for (String text : divideContents) {
            smsManager.sendTextMessage(phoneNumber, null, text, sendPendingIntent, null);
        }
    }


    /**
     * 格式化手机号码
     *
     * @param phone
     * @return
     */
    public static String getFormatedPhone(String phone) {
        String result = "";
        if (isPhoneNumber2(phone)) {
            result = String.format("%s-%s-%s", phone.substring(0, 3), phone.substring(3, 7), phone.substring(7, 11));
        } else {
            result = phone;
        }
        return result;
    }

    /**
     * 格式化手机号码
     *
     * @param phone
     * @return
     */
    public static String getFormatedPhoneWithSpace(String phone) {
        String result = "";
        if (isPhoneNumber2(phone)) {
            result = String.format("%s %s %s", phone.substring(0, 3), phone.substring(3, 7), phone.substring(7, 11));
        } else {
            result = phone;
        }
        return result;
    }

    /**
     * 获取设置SD卡是否可用
     *
     * @return
     */
    public static boolean isSDCardEnable() {
        return Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState());
    }

    /**
     * 获取设备SD卡路径
     *
     * @return
     */
    public static String getSDCardPath() {
        return Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator;
    }

    /**
     * 拨号
     *
     * @param context
     * @param phone
     */
    public static void makePhoneCall(Context context, String phone) {
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
        context.startActivity(intent);
    }

    public static String secToTime(int time) {
        String timeStr = null;
        int hour = 0;
        int minute = 0;
        int second = 0;
        if (time <= 0) {
            timeStr = "00:00";
        } else {
            minute = time / 60;
            if (minute < 60) {
                second = time % 60;
                timeStr = String.format("%s:%s", unitFormat(minute), unitFormat(second));
            } else {
                hour = minute / 60;
                minute = minute % 60;
                second = time - hour * 3600 - minute * 60;
                timeStr = String.format("%s:%s:%s", unitFormat(hour), unitFormat(minute), unitFormat(second));
            }
        }
        return timeStr;
    }

    public static String unitFormat(int data) {
        String resultStr = null;
        if (data >= 0 && data < 10) {
            resultStr = String.format("0%d", data);
        } else {
            resultStr = String.valueOf(data);
        }
        return resultStr;
    }


    /**
     * 运单号校验
     *
     * @param expressId
     * @return
     */
    public static boolean isRightExpressId(String expressId) {
        boolean result = false;
        // 不为null
        if (!TextUtils.isEmpty(expressId)) {
//            // 纯数字
//            if (isAllDigital(expressId)) {
//                // 10位数以上
//                if (expressId.length() >= 10) {
//                    return true;
//                }
//            }
            String regExp = "^[0-9A-Za-z/-]{8,32}$";
            Pattern pattern = Pattern.compile(regExp);
            Matcher matcher = pattern.matcher(expressId);
            result = matcher.matches();
        }
        return result;
    }

    /**
     * 数字与大写字母
     *
     * @param data
     * @return
     */
    public static boolean numberAndBigDigital(String data) {
        boolean result = false;
        // 不为null
        if (!TextUtils.isEmpty(data)) {
            String regExp = "^[0-9A-Z]*$";
            Pattern pattern = Pattern.compile(regExp);
            Matcher matcher = pattern.matcher(data);
            result = matcher.matches();
        }
        return result;
    }

    /**
     * @param bmp
     * @param needRecycle
     * @return
     */
    public static byte[] bmpToByteArray(final Bitmap bmp, final boolean needRecycle) {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.PNG, 100, output);
        if (needRecycle) {
            bmp.recycle();
        }

        byte[] result = output.toByteArray();
        try {
            output.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    /**
     * 校验收件地址与寄件地址、防止输入特殊字符(顺丰不支持)
     *
     * @param address
     * @return
     */
    public static boolean isWrongAddressFormat(String address) {
        boolean flag;
        String regExp = "[`~!@#$%^&*()+=|{}':;',\\\\[\\\\].\\-<>/?～！@#￥%……&*（）——+|{}【】‘；：”“’。，、？_]";
        Pattern pattern = Pattern.compile(regExp);
        Matcher matcher = pattern.matcher(address);
        flag = matcher.find();
        return flag;
    }

    /**
     * 从本地获取快递条款
     *
     * @param inputStream
     * @return
     */
    public static String getTermString(InputStream inputStream) {
        InputStreamReader inputStreamReader = null;
        try {
            inputStreamReader = new InputStreamReader(inputStream, "utf-8");
        } catch (UnsupportedEncodingException e1) {
            e1.printStackTrace();
        }
        BufferedReader reader = new BufferedReader(inputStreamReader);
        StringBuilder sb = new StringBuilder("");
        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line);
                sb.append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return sb.toString();
    }

    public static Bitmap getimage(String srcPath) {
        BitmapFactory.Options newOpts = new BitmapFactory.Options();
        //开始读入图片，此时把options.inJustDecodeBounds 设回true了
        newOpts.inJustDecodeBounds = true;
        Bitmap bitmap = BitmapFactory.decodeFile(srcPath, newOpts);
        //此时返回bm为空
        newOpts.inJustDecodeBounds = false;
        int w = newOpts.outWidth;
        int h = newOpts.outHeight;//现在主流手机比较多是800*480分辨率，所以高和宽我们设置为
        float hh = 800f;//这里设置高度为800f
        float ww = 480f;//这里设置宽度为480f
        // 缩放比。由于是固定比例缩放，只用高或者宽其中一个数据进行计算即可
        int be = 1;//be=1表示不缩放
        if (w > h && w > ww) {
            //如果宽度大的话根据宽度固定大小缩放
            be = (int) (newOpts.outWidth / ww);
        } else if (w < h && h > hh) {
            //如果高度高的话根据宽度固定大小缩放
            be = (int) (newOpts.outHeight / hh);
        }
        if (be <= 0) {
            be = 1;
        }
        newOpts.inSampleSize = be;
        //设置缩放比例//重新读入图片，注意此时已经把options.inJustDecodeBounds 设回false了
        bitmap = BitmapFactory.decodeFile(srcPath, newOpts);
        return compressImage(bitmap);//压缩好比例大小后再进行质量压缩
    }

    public static Bitmap compressImage(Bitmap image) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 100, baos);//质量压缩方法，这里100表示不压缩，把压缩后的数据存放到baos中
        int options = 100;
        while (baos.toByteArray().length / 1024 > 100 && options >= 20) {
            // 循环判断如果压缩后图片是否大于100kb,大于继续压缩
            baos.reset();//重置baos即清空
            options -= 10;//每次都减少10
            image.compress(Bitmap.CompressFormat.JPEG, options, baos);//这里压缩options%，把压缩后的数据存放到baos中
        }
        ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());//把压缩后的数据baos存放到ByteArrayInputStream中
        Bitmap bitmap = BitmapFactory.decodeStream(isBm, null, null);//把ByteArrayInputStream数据生成图片
        return bitmap;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        return inSampleSize;
    }

    public static Bitmap getSmallBitmap(String filePath) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, options);
        options.inSampleSize = calculateInSampleSize(options, 480, 800);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(filePath, options);
    }

    /**
     * 补齐零
     *
     * @param sourceDate
     * @param formatLength
     * @return
     */
    public static String fortCompWithZore(int sourceDate, int formatLength) {
        String newString = String.format("%0" + formatLength + "d", sourceDate);
        return newString;
    }

    /**
     * 获取手机屏幕宽度
     *
     * @param context
     * @return
     */
    public static int getPhoneWidth(Context context) {
        WindowManager windowManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(displayMetrics);
        return displayMetrics.widthPixels;
    }

    /**
     * 复制文字到手机剪切板中
     *
     * @param context
     * @param content
     * @param toast
     */
    public static void copyToClipboard(Context context, String content, String toast) {
        ClipboardManager cm = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        cm.setText(content);
        Utils.toast(context, toast);
    }

    /**
     * 判断通知栏是否开启
     *
     * @param context
     * @return
     */
    public static boolean isNotificationEnabled(Context context) {

        String CHECK_OP_NO_THROW = "checkOpNoThrow";
        String OP_POST_NOTIFICATION = "OP_POST_NOTIFICATION";

        AppOpsManager mAppOps = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
        ApplicationInfo appInfo = context.getApplicationInfo();
        String pkg = context.getApplicationContext().getPackageName();
        int uid = appInfo.uid;

        Class appOpsClass = null;
      /* Context.APP_OPS_MANAGER */
        try {
            appOpsClass = Class.forName(AppOpsManager.class.getName());
            Method checkOpNoThrowMethod = appOpsClass.getMethod(CHECK_OP_NO_THROW, Integer.TYPE, Integer.TYPE,
                    String.class);
            Field opPostNotificationValue = appOpsClass.getDeclaredField(OP_POST_NOTIFICATION);

            int value = (Integer) opPostNotificationValue.get(Integer.class);
            return ((Integer) checkOpNoThrowMethod.invoke(mAppOps, value, uid, pkg) == AppOpsManager.MODE_ALLOWED);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return false;
    }

}
