//package com.example.demo.IJKPlayer;
//
//import androidx.annotation.Nullable;
//import androidx.appcompat.app.AppCompatActivity;
//
//import android.content.pm.ActivityInfo;
//import android.os.Bundle;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.ImageView;
//
//import com.example.demo.R;
//import com.shuyu.gsyvideoplayer.GSYBaseActivityDetail;
//import com.shuyu.gsyvideoplayer.builder.GSYVideoOptionBuilder;
//import com.shuyu.gsyvideoplayer.utils.OrientationUtils;
//import com.shuyu.gsyvideoplayer.video.StandardGSYVideoPlayer;
//
//import org.greenrobot.eventbus.EventBus;
//
//
//public class PlayerFactoryActivity extends AppCompatActivity {
//    private StandardGSYVideoPlayer videoPlayer;
//    private OrientationUtils orientationUtils;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_player_factory);
//
////
////        detailPlayer = (StandardGSYVideoPlayer) findViewById(R.id.detail_player);
////        //增加title
////        detailPlayer.getTitleTextView().setVisibility(View.GONE);
////        detailPlayer.getBackButton().setVisibility(View.GONE);
////
////        initVideoBuilderMode();
//        initVideoView();
//    }
//
//    private void initVideoView() {
//        videoPlayer = findViewById(R.id.gsy_player);
//        orientationUtils = new OrientationUtils(this, videoPlayer);
//        shoeGSYVideoPlayer(true);
//    }
//
//
//    /**
//     * 播放视频
//     * @param show
//     */
//    private void shoeGSYVideoPlayer( boolean show){
//        try {
//
//            if (show) {
//                orientationUtils.setRotateWithSystem(false);
//                orientationUtils.setEnable(false);
//                if(videoPlayer !=null){
////                    videoPlayer.setShrinkImageRes(R.mipmap.custom_shrink_ab);
////                    videoPlayer.setEnlargeImageRes(R.mipmap.custom_enlarge_ab);
//                    videoPlayer.getBackButton().setPadding(10,10,10,10);
////                    videoPlayer.getFullscreenButton().setVisibility(View.GONE);//隐藏全屏按钮
//                    videoPlayer.setUp("https://cctv5cnch5ca.v.wscdns.com/live/cctv5_2/index.m3u8?contentid=2820180516001",true,  "cctv");
//                    videoPlayer.startPlayLogic();
////                    orientationUtils.resolveByClick();
//                }
//                //设置返回按键功能
//                videoPlayer.getBackButton().setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        videoPlayer.onVideoPause();
////                        EventBus.getDefault().post(new EventEntity(EventBusKey.clearVideo,"",null));
//                        finish();
//                    }
//                });
////                videoPlayer.setThumbImageView(view);
//                //设置全屏按键功能,这是使用的是选择屏幕，而不是全屏
//                videoPlayer.getFullscreenButton().setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
////                        EventBus.getDefault().post(new EventEntity(EventBusKey.clearNarrow,"",mBean));
//                        finish();
////                        orientationUtils.resolveByClick();
//                    }
//                });
//            }else {
//                videoPlayer.setVideoAllCallBack(null);
//                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//            }
//            ViewGroup.LayoutParams params =  videoPlayer.getLayoutParams();
//            params.height = ViewGroup.LayoutParams.MATCH_PARENT;
//            params.width = ViewGroup.LayoutParams.MATCH_PARENT;
//            videoPlayer.setLayoutParams(params);
//            orientationUtils.resolveByClick();
//        }catch (Exception e){
//
//        }
//    }
//
//    @Override
//    public void onDestroy() {
//        try {
//            if( orientationUtils != null) {
//                orientationUtils.releaseListener();
////                videoPlayer.onVideoPause();
//            }
//        }catch (Exception e){
//        }
//        super.onDestroy();
//    }
//
//
//    @Override
//    public void onBackPressed() {
//        //先返回正常状态
//        if (orientationUtils !=null && orientationUtils.getScreenType() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE) {
//            videoPlayer.getFullscreenButton().performClick();
//            return;
//        }
//        //释放所有
//        videoPlayer.setVideoAllCallBack(null);
//        super.onBackPressed();
//    }
//    @Override
//    protected void onPause() {
//        super.onPause();
//        if(videoPlayer !=null){
//            videoPlayer.onVideoPause();
//        }
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        if(videoPlayer !=null){
//            videoPlayer.onVideoResume();
//        }
//    }
//
//}