package com.example.demo.IJKPlayer

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.dou361.ijkplayer.widget.AndroidMediaController
import com.dou361.ijkplayer.widget.IjkVideoView
import com.example.demo.R
import tv.danmaku.ijk.media.player.IjkMediaPlayer

class MediaActivity : AppCompatActivity() {
    private var ijkVideoView: IjkVideoView? = null
var mUrl = ""
    companion object {
        val urls = "url"

        fun startActivity(ctx: Context, url: String) {
            val to = Intent(ctx, MediaActivity::class.java)
            to.putExtra(urls, url)
            ctx.startActivity(to)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_media)
        mUrl = getIntent().getExtras()!!.getString(urls,"");
        init()
    }


    fun  init(){
        //初始化播放库
        //初始化播放库
        IjkMediaPlayer.loadLibrariesOnce(null)
        IjkMediaPlayer.native_profileBegin("libijkplayer.so")
        var ijkMediaPlayer: IjkMediaPlayer? = null
        ijkMediaPlayer = IjkMediaPlayer()


        ijkMediaPlayer.setOption(IjkMediaPlayer.OPT_CATEGORY_PLAYER, "start-on-prepared", 0)
        ijkMediaPlayer.setOption(IjkMediaPlayer.OPT_CATEGORY_FORMAT, "http-detect-range-support", 0)

        // 黑屏

        // 黑屏
        ijkMediaPlayer.setOption(IjkMediaPlayer.OPT_CATEGORY_PLAYER, "mediacodec_mpeg4", 1)
        // 降低 播放 rtmp 播放的延迟
        // 降低 播放 rtmp 播放的延迟
        ijkMediaPlayer.setOption(1, "analyzemaxduration", 100L)
        ijkMediaPlayer.setOption(1, "probesize", 10240L)
        ijkMediaPlayer.setOption(1, "flush_packets", 1L)
        ijkMediaPlayer.setOption(4, "packet-buffering", 0L)
        //丢帧
        //丢帧
        ijkMediaPlayer.setOption(4, "framedrop", 1L)

        //硬解码造成黑屏无声

        //硬解码造成黑屏无声
        ijkMediaPlayer.setOption(IjkMediaPlayer.OPT_CATEGORY_PLAYER, "mediacodec", 1)
        ijkMediaPlayer.setOption(IjkMediaPlayer.OPT_CATEGORY_PLAYER, "mediacodec-auto-rotate", 1)
        ijkMediaPlayer.setOption(
            IjkMediaPlayer.OPT_CATEGORY_PLAYER,
            "mediacodec-handle-resolution-change",
            1
        )


        ijkMediaPlayer.setOption(IjkMediaPlayer.OPT_CATEGORY_PLAYER, "soundtouch", 1)

        // 清空DNS，因为DNS的问题报10000

        // 清空DNS，因为DNS的问题报10000
        ijkMediaPlayer.setOption(IjkMediaPlayer.OPT_CATEGORY_FORMAT, "dns_cache_clear", 1)

        //使用AndroidMediaController类控制播放相关操作

        //使用AndroidMediaController类控制播放相关操作
        val mMediaController = AndroidMediaController(this, false)
        //mMediaController.setSupportActionBar(actionBar);
        //mMediaController.setSupportActionBar(actionBar);
        ijkVideoView = findViewById(R.id.ijkplayerView)
        ijkVideoView!!.setMediaController(mMediaController)

        // 测试可用地址
        // 香港财经  rtmp://202.69.69.180:443/webcast/bshdlive-pc
        // 湖南卫视   rtmp://58.200.131.2:1935/livetv/hunantv
        // 美国2, rtmp://media3.scctv.net/live/scctv_800

        //设置要播放的直播或者视频的地址：

        // 测试可用地址
        // 香港财经  rtmp://202.69.69.180:443/webcast/bshdlive-pc
        // 湖南卫视   rtmp://58.200.131.2:1935/livetv/hunantv
        // 美国2, rtmp://media3.scctv.net/live/scctv_800
//        var url = "rtmp://202.69.69.180:443/webcast/bshdlive-pc"
//        var url = "http://video19.ifeng.com/video07/2013/11/11/281708-102-007-1138.mp4"
        //设置要播放的直播或者视频的地址：
        ijkVideoView!!.setVideoPath(mUrl)
        //开始播放
        //开始播放
        ijkVideoView!!.start()
    }


    override fun onStop() {
        super.onStop()
        if (ijkVideoView!!.isPlaying()) {
            ijkVideoView!!.stopPlayback();
            ijkVideoView!!.release(true);
        }
        IjkMediaPlayer.native_profileEnd();
    }
}