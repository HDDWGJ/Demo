package com.example.demo.newbie

import android.graphics.Color
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.app.hubert.library.HighLight
import com.app.hubert.library.NewbieGuide
import com.example.demo.R
import com.app.hubert.library.Controller

import com.app.hubert.library.OnGuideChangedListener
import java.util.*


class NewbieActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_newbie)
        var button1 = findViewById<Button>(R.id.button1);
//        setView(button1)
        setView2(button1)
    }
    private fun setView(button:Button){
        NewbieGuide.with(this)//传入activity
            .setLabel("guide1")//设置引导层标示，用于区分不同引导层，必传！否则报错
            .addHighLight(button, HighLight.Type.RECTANGLE)//添加需要高亮的view
            .setLayoutRes(R.layout.view_guide)//自定义的提示layout，不要添加背景色，引导层背景色通过setBackgroundColor()设置
            .show();//显示引导层

    }
    private fun setView2(button:Button){
        var button1 = findViewById<Button>(R.id.button2);
        val controller = NewbieGuide.with(this)
            .setOnGuideChangedListener(object : OnGuideChangedListener {
                //设置监听
                override fun onShowed(controller: Controller) {
                    //引导层显示
                }

                override fun onRemoved(controller: Controller) {
                    //引导层消失
                    controller.remove() //移除引导层
                }
            })
            .addHighLight(button)
            .addHighLight(button1)
            .setBackgroundColor(Color.BLACK) //设置引导层背景色，建议有透明度，默认背景色为：0xb2000000
            .setEveryWhereCancelable(false) //设置点击任何区域消失，默认为true
            .setLayoutRes(R.layout.view_guide, R.id.text1) //自定义的提示layout,第二个可变参数为点击隐藏引导层view的id
//            .setOnLayoutInflatedListener(Objects:Layout)
            .alwaysShow(true) //是否每次都显示引导层，默认false
            .setLabel("guide1")
            .build() //构建引导层的控制器


        controller.remove()
        controller.show() //显示引导层

    }
}