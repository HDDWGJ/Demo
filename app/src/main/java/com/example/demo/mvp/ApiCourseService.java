package com.example.demo.mvp;

import com.example.moduledframe.net.ResponseResult;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface ApiCourseService {

    /**
     * 获取赛程时间和第几轮
     */
    @POST("user/login")
     Observable<ResponseResult<List<Object>>> getTestList(@Body RequestBody requestBody);

}
