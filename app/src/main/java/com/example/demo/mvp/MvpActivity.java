package com.example.demo.mvp;

import androidx.annotation.Nullable;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.alibaba.android.arouter.launcher.ARouter;
import com.example.demo.R;
import com.example.moduledframe.base.BaseActivity;
import com.example.moduledframe.net.ResponseResult;
import com.example.moduledframe.net.interceptor.DefaultObserver;
import com.example.moduleh5.h5.H5WebActivity;

import java.util.List;

public class MvpActivity extends BaseActivity {

    @Override
    public int initView(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        return R.layout.activity_mvp;
    }

    @Override
    protected void initData() {
     TextView textView =  findViewById(R.id.textTitle);
     textView.setText("自定义标题");


        findViewById(R.id.qingqiu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                okhttp();
            }
        });

        findViewById(R.id.html5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                H5WebActivity.Companion.startActivity(getBaseContext(),"https://www.baidu.com/","wode标题");
            }
        });
        findViewById(R.id.User).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ARouter.getInstance().build("/user/UserActivity").navigation();
            }
        });
    }

    private void okhttp(){
        CourseRetrofit.getCourse(new DefaultObserver<List<Object>>() {
            @Override
            public void onSuccess(ResponseResult<List<Object>> result) {

            }

            @Override
            public void onException(int code, String eMsg) {

            }
        },"");
    }
}