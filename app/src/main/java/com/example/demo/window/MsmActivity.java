package com.example.demo.window;

import androidx.appcompat.app.AppCompatActivity;

import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.example.demo.R;

public class MsmActivity extends AppCompatActivity {

    public static final int MSG_RECEIVER_CODE = 1;
    private EditText smsEt;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_RECEIVER_CODE:
                    String message= (String) msg.obj;
                    smsEt.setText(message);
                    smsEt.setSelection(message.length());
                    break;
                default:
                    break;
            }
        }
    };
    private MySmsObserver mySmsObserver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_msm);
        smsEt = (EditText) findViewById(R.id.smsEt);
        mySmsObserver = new MySmsObserver(this, handler);
        Uri uri=Uri.parse("content://sms");
        getContentResolver().registerContentObserver(uri,true, mySmsObserver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        getContentResolver().unregisterContentObserver(mySmsObserver);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }
}
