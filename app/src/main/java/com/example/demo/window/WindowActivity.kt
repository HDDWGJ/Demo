package com.example.demo.window

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.PixelFormat
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import com.example.demo.R
import java.lang.reflect.Method


class WindowActivity : AppCompatActivity() {
      var view : CustomViewGroup? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_window)
//        preventStatusBarExpansion(this)
        sendBroadcast(Intent("android.intent.action.STATUSBAR_DISABLE"))
        preventStatusBarExpansion(this)
    }




    @SuppressLint("WrongConstant")
    override fun onWindowFocusChanged(hasFocus: Boolean) {
        // TODO Auto-generated method stub
        super.onWindowFocusChanged(hasFocus)
        try {
            val service = getSystemService("statusbar")
            val statusbarManager = Class.forName("android.app.StatusBarManager")
            val test: Method = statusbarManager.getMethod("collapse")
            test.invoke(service)
        } catch (ex: java.lang.Exception) {
            ex.printStackTrace()
        }
    }




    val STATUS_BAR_HEIGHT = "status_bar_height"
    val DIMEN = "dimen"
    val DEF_PACKAGE = "android"

    fun preventStatusBarExpansion(context: Context) {
        val manager = context.getApplicationContext().getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val localLayoutParams = WindowManager.LayoutParams()
        localLayoutParams.type = WindowManager.LayoutParams.TYPE_SYSTEM_ERROR
        localLayoutParams.gravity = Gravity.TOP
        localLayoutParams.flags = (WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                or WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                or WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN)
        localLayoutParams.width = WindowManager.LayoutParams.MATCH_PARENT
        val resId: Int = context.getResources()
                .getIdentifier(STATUS_BAR_HEIGHT, DIMEN, DEF_PACKAGE)
        val result: Int
        result = if (resId > 0) {
            context.getResources().getDimensionPixelSize(resId)
        } else {
            // Use Fallback size:
            60 // 60px Fallback
        }
        localLayoutParams.height = result
        localLayoutParams.format = PixelFormat.TRANSPARENT
        if (view == null) {
            view = CustomViewGroup(context)
        }
        try {
            manager?.addView(view, localLayoutParams)
        } catch (ignored: Exception) {
        }
    }

    fun allowStatusBarExpansion(context: Context) {
        val manager = context.getApplicationContext()
                .getSystemService(Context.WINDOW_SERVICE) as WindowManager
        try {
            manager?.removeViewImmediate(view)
        } catch (ignored: Exception) {
        }
    }

    class CustomViewGroup(context: Context?) : ViewGroup(context) {
        override fun onLayout(changed: Boolean,
                              l: Int,
                              t: Int,
                              r: Int,
                              b: Int) {
        }

        override fun onInterceptTouchEvent(ev: MotionEvent): Boolean {
            // Intercepted touch!
            return true
        }
    }
}