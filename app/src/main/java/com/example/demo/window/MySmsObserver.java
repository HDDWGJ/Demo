package com.example.demo.window;

import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Android Studio.
 * User: wsl
 * Date: 2021/1/13
 * Time: 8:31 PM
 * Description:
 */
public class MySmsObserver extends ContentObserver {

    private Context mContext;
    private Handler mHandler;


    public MySmsObserver(Context context, Handler handler) {
        super(handler);
        this.mContext = context;
        this.mHandler = handler;
    }

    @Override
    public void onChange(boolean selfChange, Uri uri) {
        super.onChange(selfChange, uri);

        if (uri.toString().equals("content://sms/raw")) {
            return;
        }

        Uri queryUri = Uri.parse("content://sms/inbox");
        String code = "";
        Cursor cursor = mContext.getContentResolver().query(queryUri, null, null, null, "date desc");
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                String address = cursor.getString(cursor.getColumnIndex("address"));
                String message = cursor.getString(cursor.getColumnIndex("body"));

                // TODO: 2015/9/28 这里可以根据address做一些自己的判断，比如只有特定的手机号才做判断

                Log.e("guxuewu", "address:==>" + address + " message:==>" + message);

                // TODO: 2015/9/28 这里可以根据自己的项目进行特定的正则表达式的编写
                Pattern pattern = Pattern.compile("(\\d{6})");
                Matcher matcher = pattern.matcher(message);

                if (matcher.find()) {
                    code = matcher.group(0);
                    mHandler.obtainMessage(MsmActivity.MSG_RECEIVER_CODE, code).sendToTarget();
                }
            }

            cursor.close();
        }


    }
}
