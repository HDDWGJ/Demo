package com.example.demo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.app.SkinAppCompatDelegateImpl;

import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.ethanhua.skeleton.SkeletonScreen;
import com.example.customview.activity.CollectionActivity;
import com.example.customview.activity.CustomActivity;
import com.example.customview.activity.PagingActivity;
import com.example.customview.activity.ScrollActivity;
import com.example.demo.IJKPlayer.IJKPlayerActivity;
import com.example.demo.Sensor.SensorActivity;
import com.example.demo.editText.EditTextActivity;
import com.example.demo.getListAPP.ListAppActivity;
import com.example.demo.gettime.GetTimeActivity;
import com.example.demo.mvp.MvpActivity;
import com.example.demo.nestedScroll.NestedActivity;
import com.example.demo.newbie.NewbieActivity;
import com.example.demo.shortcut.ShortcutActivity;
import com.example.demo.tabView.TabView2Activity;
import com.example.demo.thread.ThreadActivity;
import com.example.demo.util.VoiceUtils;
import com.example.demo.window.MsmActivity;
import com.example.demo.window.WindowActivity;
import com.example.demo.xcx.XiaoCXActivity;

import java.lang.ref.WeakReference;

import skin.support.SkinCompatManager;

public class MainActivity extends AppCompatActivity  {


    Button huanfu,huifu,text,getTime,edit,listapp,sensor,thread,mCostomView,xcxview;
    private SkeletonScreen skeletonScreen;// 骨架屏
    private LinearLayout rootView;


    public static class MyHandler extends android.os.Handler {
        private final WeakReference<MainActivity> activityWeakReference;

        MyHandler(MainActivity activity) {
            this.activityWeakReference = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (activityWeakReference.get() != null) {
                activityWeakReference.get().skeletonScreen.hide();
            }
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        huanfu = findViewById(R.id.huanfu);



        findViewById(R.id.mvpjichu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), MvpActivity.class));
            }
        });

         mCostomView = findViewById(R.id.costomView);
        mCostomView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), CustomActivity.class));
            }
        });
        findViewById(R.id.player).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), IJKPlayerActivity.class));
            }
        });
        findViewById(R.id.costomView2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), ScrollActivity.class));
            }
        });
        findViewById(R.id.costomView3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), PagingActivity.class));
            }
        });
        findViewById(R.id.costomView4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), CollectionActivity.class));
            }
        });
        findViewById(R.id.newbie).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), NewbieActivity.class));
            }
        });

        findViewById(R.id.tabView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), TabView2Activity.class));
            }
        });

        sendBroadcast(new Intent("android.intent.action.STATUSBAR_DISABLE"));
        huanfu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SkinCompatManager.getInstance().loadSkin("night", SkinCompatManager.SKIN_LOADER_STRATEGY_BUILD_IN);
//                SkinCompatManager.getInstance().loadSkin("night", SkinCompatManager.SKIN_LOADER_STRATEGY_BUILD_IN);
//                SkinCompatManager.getInstance().loadSkin("night", null, SkinCompatManager.SKIN_LOADER_STRATEGY_BUILD_IN);
//                // 指定皮肤插件
//                SkinCompatManager.getInstance().loadSkin("new.skin",this, 1);

            }
        });
        huifu = findViewById(R.id.huifu);
        huifu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 恢复应用默认皮肤
                SkinCompatManager.getInstance().restoreDefaultTheme();
            }
        });
        text = findViewById(R.id.text);
        text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), TaberActivity.class));
            }
        });
        getTime = findViewById(R.id.get_time);
        getTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), GetTimeActivity.class));
            }
        });
        edit = findViewById(R.id.edit);
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), EditTextActivity.class));
            }
        });
        listapp  = findViewById(R.id.listapp);
        listapp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), ListAppActivity.class));
            }
        });
        Button scroll = findViewById(R.id.scroll);
        scroll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), NestedActivity.class));
            }
        });
        sensor = findViewById(R.id.sensor);
        sensor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), SensorActivity.class));
            }
        });
        thread = findViewById(R.id.thread);
        thread.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), ThreadActivity.class));
            }
        });


        Button    window = findViewById(R.id.window);
        window.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), WindowActivity.class));
            }
        });

        Button windowmsm = findViewById(R.id.windowmsm);
        windowmsm .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), MsmActivity.class));
            }
        });

        xcxview = findViewById(R.id.xcxview);
        xcxview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), XiaoCXActivity.class));
            }
        });


        Button Shortcut = findViewById(R.id.Shortcut);
        Shortcut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), ShortcutActivity.class));
            }
        });

        Button voiceButton = findViewById(R.id.voice);
        voiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                VoiceUtils.with(getBaseContext()).play("1223","in");
            }
        });

//        骨架屏幕
//        rootView = findViewById(R.id.rootView);
//        skeletonScreen = Skeleton.bind(rootView)
//                .load(R.layout.activity_view_skeleton)
//                .duration(1000)
//                .color(R.color.shimmer_color)
//                .angle(0)
//                .show();
//
//        MyHandler myHandler = new MyHandler(this);
//        myHandler.sendEmptyMessageDelayed(1, 5000);

    }



    @NonNull
    @Override
    public AppCompatDelegate getDelegate() {
        return SkinAppCompatDelegateImpl.get(this, this);
    }



}
