package com.example.demo.tabView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.demo.R;

import java.util.ArrayList;
import java.util.List;

public class TabView2Activity extends AppCompatActivity {



    private ViewPager viewpager;
    private List<ImageView> list;// 装ImageView的集合
    private ImageView imageView;
    private ImageView[] tips;// 装点点的集合
    private ImageView dianimageView;
    private ViewGroup viewGroup;
    private int mPageSize = 3;
    private Button laftbutton,rightbutton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab_view2);

        viewpager = (ViewPager) findViewById(R.id.viewPager);
        viewGroup = (ViewGroup) findViewById(R.id.viewGroup);

        laftbutton = findViewById(R.id.laftbutton);
        rightbutton = findViewById(R.id.rightbutton);

        // 将imageView图片资源存在集合中
        list = new ArrayList<ImageView>();
        for (int i = 0; i < mPageSize; i++) {
            imageView = new ImageView(this);
            imageView.setBackgroundResource(R.mipmap.icon_male_img);
            // 为imageView设置tag，将id传入，方便页面跳转时，发送图片的资源id
            imageView.setTag(R.mipmap.icon_male_img);
            list.add(imageView);
        }

        // 将点点装入到结集合中
        tips = new ImageView[mPageSize];
        for (int i = 0; i < mPageSize; i++) {
            dianimageView = new ImageView(this);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.setMargins(15, 15, 15, 15);
            dianimageView.setLayoutParams(params);
            dianimageView.setPadding(10,10,10,10);

            tips[i] = dianimageView;
            if (i == 0) {
                tips[i].setBackgroundResource(R.mipmap.icon_page_select);
            } else {
                tips[i].setBackgroundResource(R.mipmap.icon_page_unselect);
            }

            viewGroup.addView(dianimageView);
        }

        // 设置适配器
        viewpager.setAdapter(new MyAdapter(list, this));
        viewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            // 当滑动状态改变时调用
            public void onPageSelected(int arg0) {
                setImageBackground(arg0 % list.size());
            }
            // 当前页面被滑动时调用
            public void onPageScrolled(int arg0, float arg1, int arg2) {

            }
            // 当新的页面被选中时调用
            public void onPageScrollStateChanged(int arg0) {

            }
        });

        // 第一次初始化界面时，显示的界面
        // 设置ViewPager的默认项, 设置为长度的100倍，这样子开始就能往左滑动
//        viewpager.setCurrentItem((tips.length) * 100);

        rightbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("viewPager","viewPagerPoint"+ (viewpager.getCurrentItem() + 1));
                viewpager.setCurrentItem(viewpager.getCurrentItem() + 1);

            }
        });
        laftbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("viewPager","viewPagerPoint"+ (viewpager.getCurrentItem() - 1));
                viewpager.setCurrentItem(viewpager.getCurrentItem() - 1);

            }
        });

    }

    /**
     * 设置选中的tip的背景 ，选中哪个图片，哪个图片的点点就变成白的
     */
    private void setImageBackground(int selectItems) {

        for (int i = 0; i < tips.length; i++) {
            if (i == selectItems) {
                tips[i].setBackgroundResource(R.mipmap.icon_page_select);
            } else {
                tips[i].setBackgroundResource(R.mipmap.icon_page_unselect);
            }
        }
    }



    private void initView(){

    }
}