package com.example.demo.getListAPP;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.example.demo.R;

import java.util.List;

/**
 * 我的收藏
 */
public class ListAdapter extends RecyclerView.Adapter<ListAdapter.MyViewHolder> {
    private  List<IconBean> mList;
    private Context context;


    public ListAdapter( List<IconBean> List, Context context){
        this.mList=List;
        this.context=context;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(context).inflate(R.layout.item_collection,parent,false);
        return new MyViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        IconBean bean= mList.get(position);

       holder.name.setText(bean.getAppName());
       holder.imageView.setImageDrawable(bean.getAppIcon());
      
    }


    @Override
    public int getItemCount() {
        return mList.size();
    }



    //继承RecyclerView.ViewHolder抽象类的自定义ViewHolder
     class MyViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        TextView title;
        TextView content,name;
        public MyViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.imageView);
            title  = itemView.findViewById(R.id.title);
            content = itemView.findViewById(R.id.content);
            name = itemView.findViewById(R.id.name);

        }
    }


}
