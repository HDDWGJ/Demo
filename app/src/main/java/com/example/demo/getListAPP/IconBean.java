package com.example.demo.getListAPP;

import android.graphics.drawable.Drawable;

public class IconBean {
    private String AppName;
    private Drawable AppIcon;
    private Boolean IsSystemApp;

    public Boolean getSystemApp() {
        return IsSystemApp;
    }

    public void setSystemApp(Boolean systemApp) {
        IsSystemApp = systemApp;
    }

    public String getAppName() {
        return AppName;
    }

    public void setAppName(String appName) {
        AppName = appName;
    }

    public Drawable getAppIcon() {
        return AppIcon;
    }

    public void setAppIcon(Drawable appIcon) {
        AppIcon = appIcon;
    }
}
