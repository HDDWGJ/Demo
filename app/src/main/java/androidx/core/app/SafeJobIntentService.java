package androidx.core.app;

import android.content.Intent;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.RestrictTo;

/**
 * Created by Android Studio.
 * User: wsl
 * Date: 2021/12/7
 * Time: 2:33 下午
 * Description:
 */
@RestrictTo({RestrictTo.Scope.LIBRARY})
public class SafeJobIntentService extends JobIntentService {
    public SafeJobIntentService() {
    }

    GenericWorkItem dequeueWork() {
        try {
            return super.dequeueWork();//1 这里我们对此方法进行了try/catch操作
        } catch (SecurityException var2) {
            var2.printStackTrace();
            return null;
        }
    }

    public void onCreate() {
        super.onCreate();
        if (Build.VERSION.SDK_INT >= 26) {
            this.mJobImpl = new SafeJobServiceEngineImpl(this);
        } else {
            this.mJobImpl = null;
        }
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {

    }


}
