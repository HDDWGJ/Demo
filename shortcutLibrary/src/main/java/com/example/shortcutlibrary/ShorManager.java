package com.example.shortcutlibrary;


import com.example.shortcutlibrary.imp.ShortcutImp;
import com.example.shortcutlibrary.shortcut.NewShortcut;


import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Constructor;

/**
 * Created by Android Studio.
 * User: wsl
 * Date: 2021/11/10
 * Time: 6:02 下午
 * Description:
 */
public class ShorManager {

    private  static ShortcutImp mSingleInstance;

    private ShorManager(){}

    public static ShortcutImp getIntance() {
        if (mSingleInstance == null) {
            synchronized (ShortcutImp.class) {
                if (mSingleInstance == null) {
                    mSingleInstance = new NewShortcut();
                }
            }
        }

        return mSingleInstance;
    }
}
