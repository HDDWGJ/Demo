package com.example.shortcutlibrary.shortcut;

import android.annotation.TargetApi;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ShortcutInfo;
import android.content.pm.ShortcutManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Icon;
import android.os.Build;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.example.shortcutlibrary.imp.ShortcutImp;
import com.example.shortcutlibrary.util.PinnedReceiver;

/**
 * Created by Android Studio.
 * User: wsl
 * Date: 2021/10/26
 * Time: 10:56 上午
 * Description:
 */
public class NewShortcut implements ShortcutImp {

    private ShortcutManager mShortcutManager;




    @Override
    public void newShortcutActivity(@NonNull Context context, @NonNull String url, @NonNull String cls, @NonNull String name) {
        downShortcutICon(context,url,cls,name);
    }

    /**
     *  下载快捷方式icon
     */
    private   void downShortcutICon(Context context,String url, String cls,String name) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            final Bitmap[] bitmap = new Bitmap[1];
            Glide.with(context).load(url).asBitmap().into(new SimpleTarget<Bitmap>() {
                @Override
                public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                    bitmap[0] = (Bitmap) resource;
                    if (bitmap[0] != null) {
                        createImgPinnedShortcut(bitmap[0], context, cls,name);
                    }
                }
            });
        }

    }
    /**
     * 创建urlImg固定快捷方式
     */
    @TargetApi(Build.VERSION_CODES.O)
    private  void createImgPinnedShortcut(Bitmap bitmap,Context context, String clsPath,String name) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (mShortcutManager == null) {
                mShortcutManager = context.getSystemService(ShortcutManager.class);
            }
            if (mShortcutManager.isRequestPinShortcutSupported()) {
                try {
                    Class clas =  Class.forName(clsPath);

                Intent intent = new Intent(context,clas);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT);
                intent.addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                intent.setAction("com.example.shortcutlibrary.NAVIGATION");
                ShortcutInfo pinShortcutInfo = new ShortcutInfo.Builder(context, name)
                        .setShortLabel(name)
                        .setLongLabel(name)
//                        .setIcon(Icon.createWithResource(this, R.drawable.ic_navigation))
                        .setIcon(Icon.createWithBitmap(bitmap))
                        .setIntent(intent)
                        .build();


                // 注册固定快捷方式成功广播
                IntentFilter intentFilter = new IntentFilter();
                intentFilter.addAction("com.example.shortcutlibrary.util.PINNED_BROADCAST");

                PinnedReceiver receiver = new PinnedReceiver();
                context.registerReceiver(receiver, intentFilter);

                Intent pinnedShortcutCallbackIntent = new Intent("com.example.shortcutlibrary.util.PINNED_BROADCAST");
                PendingIntent successCallback = PendingIntent.getBroadcast(context, 0,
                        pinnedShortcutCallbackIntent, 0);
                mShortcutManager.requestPinShortcut(pinShortcutInfo, successCallback.getIntentSender());
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }else {
//            Toast.makeText(this,"设备不支持在桌面创建快捷图标",Toast.LENGTH_SHORT).show();
        }

    }


}
