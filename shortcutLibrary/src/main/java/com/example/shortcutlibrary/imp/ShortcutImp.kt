package com.example.shortcutlibrary.imp

import android.content.Context

/**
 * Created by Android Studio.
 * User: wsl
 * Date: 2021/11/10
 * Time: 5:26 下午
 * Description:
 */
interface ShortcutImp {
    /**
     * 创建快捷打开地址
     */
     fun newShortcutActivity(context: Context,  url:String,  cls:String,  name:String)
}