package com.example.shortcutlibrary.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.shortcutlibrary.R

class ShortcutHomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shortcut_home)
    }
}