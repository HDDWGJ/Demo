package com.example.moduleuser

import com.example.moduledframe.base.BaseActivity
import com.example.moduledframe.mvpbase.presenter.BaseNullKtPresenter
import android.os.Bundle
import com.alibaba.android.arouter.facade.annotation.Route
import com.alibaba.android.arouter.launcher.ARouter
import kotlinx.android.synthetic.main.activity_user.*

@Route(path = "/user/UserActivity")
class UserActivity : BaseActivity<BaseNullKtPresenter>() {

    override fun initView(savedInstanceState: Bundle?): Int {
        return R.layout.activity_user
    }

    override fun initData() {
        button1.setOnClickListener {
            ARouter.getInstance().build("/h5/H5WebActivity").navigation()
        }
    }
}