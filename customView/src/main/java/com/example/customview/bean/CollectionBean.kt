package com.example.customview.bean

/**
 * Created by Android Studio.
 * User: wsl
 * Date: 7/14/21
 * Time: 4:29 PM
 * Description:
 */
data class CollectionBean(  var type: Int,
                            var segmentData: String,
                            var listData: String,
                            var scrollData: String)

