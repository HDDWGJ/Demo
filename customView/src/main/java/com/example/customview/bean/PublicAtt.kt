package com.example.customview.bean

import java.io.Serializable

/**
 * Created by Android Studio.
 * User: wsl
 * Date: 7/20/21
 * Time: 11:07 AM
 * Description:
 */
data class PublicAtt(var cornerRadius:Int,var borderColor:String,var borderWidth:Int,var backgroundColor:String): Serializable
