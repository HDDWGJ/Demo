package com.example.customview.bean

/**
 * Created by Android Studio.
 * User: wsl
 * Date: 7/20/21
 * Time: 11:05 AM
 * Description:
 */
data  class LayoutBean (var left:Int,var top:Int,var right:Int,var bottom:Int,var height:Int,var width:Int)