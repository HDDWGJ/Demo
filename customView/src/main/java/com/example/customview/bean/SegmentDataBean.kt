package com.example.customview.bean

import java.io.Serializable
import java.util.*

/**
 * Created by Android Studio.
 * User: wsl
 * Date: 7/20/21
 * Time: 4:08 PM
 * Description:整块版面数据模型
 */
    data class SegmentDataBean(var title:String, var isRefresh:Boolean, var headerHeight:Float, var pinSectionHeaderVerticalOffset:Float,
                               var header:MutableList<TableBean.ViewBean>, var segmented:Segmented, var list:MutableList<ListBean>){
        //头部数据
        data class Header(var id:Int){}

//    "titleNormalFont":13,
//    "titleSelectedFont":13,
//    "titleNormalWeight":"regular",
//    "titleSelectedWeight":"regular",
//    "titleNormalColor":"#555555",
//    "titleSelectedColor":"#2877FF",
//    "isItemSpacingAverageEnabled":false,
//    "contentEdgeInsetLeft":20,
//    "contentEdgeInsetRight":0,
//    "isMaskLine": true,
//    "indicatorHeight":24,
//    "indicatorWidth":null,
//    "indicatorWidthIncrement":20,
//    "indicatorCornerRadius":12,
//    "indicatorColor":"#DFEBFF",
        // 滑动控制器属性
        data class Segmented(var titleNormalFont:Float,var titleSelectedFont:Float,var titleNormalWeight:String,var titleSelectedWeight:String,var titleNormalColor:String,
                             var isItemSpacingAverageEnabled:Boolean,var contentEdgeInsetLeft:Float,var contentEdgeInsetRight:Float,var isMaskLine:Boolean,
                             var indicatorHeight:Float,var indicatorWidth:Float,var indicatorWidthIncrement:Float,var indicatorCornerRadius:Float,var indicatorColor:String,
                             var titleSelectedColor:String){}


        //内部也没数据
        data class ListBean(var isDefalut:Boolean,var title:String,var segmented:Segmented,var spacing:Int,var publicAtt:PublicAtt,
                            var segmentTitles:MutableList<String>,var controllers:MutableList<TableBean>)
    }


