package com.example.customview.bean

/**
 * Created by Android Studio.
 * User: wsl
 * Date: 7/14/21
 * Time: 4:28 PM
 * Description:
 */
data class PagingBean( var type: Int,
                       var listData: String,
                       var cardData: String,
                       var scrollData: String)


