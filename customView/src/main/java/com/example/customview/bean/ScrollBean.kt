package com.example.customview.bean

/**
 * Created by Android Studio.
 * User: wsl
 * Date: 7/14/21
 * Time: 4:27 PM
 * Description:
 */
data class ScrollBean(
                      var type: Int,
                      var segmentData: String,
                      var listData: String,
                      var cardData: String
)

