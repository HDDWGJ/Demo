package com.example.customview.adapter

import android.content.Context
import android.widget.Toast
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.customview.R
import com.example.customview.base.BaseRecyclerAdapter
import com.example.customview.base.RecyclerViewHolder
import com.example.customview.bean.TableBean
import com.example.customview.util.UIUtils


/**
 * Created by Android Studio.
 * User: wsl
 * Date: 7/15/21
 * Time: 11:23 AM
 * Description:
 */
class ListAdapter(private val mContext: Context, var beans: MutableList<TableBean.ViewBean>) : BaseRecyclerAdapter<String>(mContext) {

    override fun getLayoutId(viewType: Int): Int {
        return R.layout.item_list_view
    }

    override fun bindView(holder: RecyclerViewHolder?, position: Int) {

        val relatve= holder?.getView<CoordinatorLayout?>(R.id.item)
        relatve?.removeAllViews()
            for (bean:TableBean.ViewBean in beans){
                when(bean.type){
                    0 -> {
                        relatve?.addView(UIUtils.addTextView(mContext, bean.labelData))
                    }
                    1 -> {
                        relatve?.addView(UIUtils.addButtonView(mContext, bean.imageData)) //这里赋值还需要改动
                    }
                    2 -> {
                        relatve?.addView(UIUtils.addImgView(mContext, bean.imageData))
                        //                relatve?.layoutParams = UIUtils.setLayoutParams(context,0,0, 0, 0)
                    }
                    3 -> {
                        relatve?.addView(UIUtils.addVIew(context, bean.viewData))
                    }
                }
            }
        relatve?.setOnClickListener { Toast.makeText(mContext, "aaa$position", Toast.LENGTH_LONG) }
    }


}