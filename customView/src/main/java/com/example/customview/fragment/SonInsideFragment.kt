package com.example.customview.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.fragment.app.Fragment
import com.example.customview.R
import com.example.customview.bean.SegmentDataBean
import com.example.customview.bean.TableBean
import com.example.customview.util.LayoutUtils


/**
 *内部fragment展示
 */
class SonInsideFragment(val contexts: Context, val bean :  MutableList<TableBean>) : Fragment() {
    var mContext:Context? = null
    var mBean : MutableList<TableBean>? = null


    var fragment: FrameLayout?=null
    companion object {

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mContext = contexts
        this.mBean = bean
        return inflater.inflate(R.layout.fragment_son, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        try {
            fragment = view.findViewById(R.id.fragment)
            fragment?.addView(LayoutUtils.addRecyclerView(mContext!!,mBean!!))

        }catch (e:Exception){
            e.printStackTrace()
        }


    }

}