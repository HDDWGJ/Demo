package com.example.customview.util

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable

/**
 * Created by Android Studio.
 * User: wsl
 * Date: 7/16/21
 * Time: 12:29 PM
 * Description:
 */
class DrawableUtil {
    companion object instance {

        @SuppressLint("WrongConstant")
        fun getDrawable(rgb: Int, radius: Int, context: Context): Drawable? {
            val gradientDrawable = GradientDrawable()
            gradientDrawable.setColor(rgb)
            gradientDrawable.gradientType = GradientDrawable.RECTANGLE //矩形
            gradientDrawable.cornerRadius = radius.toFloat() //四周圆角半径
            gradientDrawable.setStroke(dp2px(context, 1f), rgb) //边框厚度与颜色
            return gradientDrawable
        }

        fun dp2px(context: Context, dipValue: Float): Int {
            val scale = context.resources.displayMetrics.density
            return (dipValue * scale + 0.5f).toInt()
        }


    }
}