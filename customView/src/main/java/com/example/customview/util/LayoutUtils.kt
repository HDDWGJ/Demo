package com.example.customview.util

import android.annotation.SuppressLint
import android.content.Context
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.example.customview.R
import com.example.customview.adapter.ListAdapter
import com.example.customview.bean.SegmentDataBean
import com.example.customview.bean.TableBean
import com.example.customview.fragment.SonFragment
import com.example.customview.fragment.SonInsideFragment
import com.google.android.material.tabs.TabLayout

/**
 * Created by Android Studio.
 * User: wsl
 * Date: 7/22/21
 * Time: 4:37 PM
 * Description:
 */
class LayoutUtils {
    companion object instance {

        /**
         * 添加ITEM UI
         */
        @SuppressLint("NewApi")
        fun  addRecyclerView(mContext:Context,bean : MutableList<TableBean>):RelativeLayout{
            var constart =  setConstraintLayout(mContext)
            val recyclerView = RecyclerView(mContext?.applicationContext!!)

            recyclerView.id = R.id.recyclerView
            val tvRightLayoutParams = UIUtils.setMatlayoutParams()

            tvRightLayoutParams.marginStart =16// ScreenUtil.dpToPx(this, 16)
            tvRightLayoutParams.topMargin = 16//ScreenUtil.dpToPx(this, 16)

            recyclerView.layoutParams = tvRightLayoutParams
            constart.addView(recyclerView)

            InitAdapter(recyclerView,bean,mContext)
            return constart
        }

        private  fun setConstraintLayout(mContext:Context): RelativeLayout {
            val constraintLayout = RelativeLayout(mContext)

            constraintLayout.id = R.id.clRoot
            constraintLayout.layoutParams = ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
            )
            return constraintLayout
        }

       private fun InitAdapter(recycler: RecyclerView,mBean : MutableList<TableBean>,mContext:Context){
            var mList :  MutableList<String> =ArrayList()

            if(mBean !=null && mBean?.size !=0){
                var mListAdapter = ListAdapter(mContext?.applicationContext!!, mBean?.get(0)?.listData?.items!!)
                val layoutManager = GridLayoutManager(mContext, 1)
                recycler.layoutParams = UIUtils.setMatlayoutParams()
                recycler.layoutManager = layoutManager
                recycler.adapter = mListAdapter

                for (index in 1..10){
                    mList.add(index.toString())
                }
                mListAdapter.data = mList
                mListAdapter?.notifyDataSetChanged()
            }
        }


        /**
         * 设置整块layout UI
         */
        fun addFragmentView(bean: SegmentDataBean,mContext: Context,mFragmentManager : FragmentManager): CoordinatorLayout {
            var tabLayout = UIUtils.addTabLayout(mContext,0f,bean.segmented)
            var viewPager =  UIUtils.addViewPager(mContext,R.id.viewPager)

            var layout =  UIUtils.setConstraintLayout(mContext)
            layout?.addView(setViewItem(mContext,bean.header))//添加顶部数据

            layout?.addView(tabLayout)
            layout?.addView(viewPager)

            var coord = UIUtils.setCoordinatorLayout(mContext)

            coord.addView(layout)
            setTabLayout(tabLayout, viewPager, bean.list,mContext,mFragmentManager)
            return coord
        }

        fun setViewItem(mContext: Context, beans: MutableList<TableBean.ViewBean>):CoordinatorLayout{
            val coordinator= CoordinatorLayout(mContext)
            coordinator.layoutParams = UIUtils.setWplayoutParams()
            for ( bean: TableBean.ViewBean in beans){
                when(bean.type){
                    0 -> {
                        coordinator?.addView(UIUtils.addTextView(mContext, bean.labelData))
                    }
                    1 -> {
                        coordinator?.addView(UIUtils.addButtonView(mContext, bean.imageData)) //这里赋值还需要改动
                    }
                    2 -> {
                        coordinator?.addView(UIUtils.addImgView(mContext, bean.imageData))
                    }
                    3 -> {
                        coordinator?.addView(UIUtils.addVIew(mContext,bean.viewData))
                    }
                }
            }
            return coordinator
        }

        /**
         * 设置tabLayout 联动
         */
        fun setTabLayout(tabLayout: TabLayout, viewPager: ViewPager, mList: MutableList<SegmentDataBean.ListBean>,mContext: Context,mFragmentManager : FragmentManager){
            tabLayout.tabGravity = TabLayout.GRAVITY_FILL
            tabLayout.tabMode = TabLayout.MODE_FIXED

            var fragmentList: MutableList<Fragment> = ArrayList()
            var tabs: MutableList<String> = ArrayList()

            for (bean in mList){
                fragmentList.add(SonFragment(mContext,bean))
                tabs.add(bean.title)
            }

            viewPager.findViewById<ViewPager>(R.id.viewPager).adapter = object : FragmentPagerAdapter(mFragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
                override fun getItem(position: Int): Fragment {
                    return fragmentList[position]
                }
                override fun getCount(): Int {
                    return fragmentList.size
                }
                override fun getPageTitle(position: Int): CharSequence? {
                    return tabs[position]
                }
            }
            //设置TabLayout和ViewPager联动
            tabLayout.setupWithViewPager(viewPager, false)

        }

        /**
         * 添加子fragment 
         */
        fun addFragmentSonView(bean:SegmentDataBean.ListBean, mContext: Context, mFragmentManager : FragmentManager): CoordinatorLayout {
            var tabLayout = UIUtils.addTabLayout(mContext,0f,bean.segmented)
            var viewPager =  UIUtils.addViewPager(mContext,R.id.viewPager1)

            var layout =  UIUtils.setConstraintLayout(mContext)
            layout?.addView(tabLayout)
            layout?.addView(viewPager)
            var coord = UIUtils.setCoordinatorLayout(mContext)

            coord.addView(layout)
            setSonTabLayout(tabLayout, viewPager, bean,mContext,mFragmentManager)
            return coord
        }

        /**
         * 添加子view的
         */
        fun setSonTabLayout(tabLayout: TabLayout, viewPager: ViewPager, mList: SegmentDataBean.ListBean, mContext: Context, mFragmentManager : FragmentManager){
            tabLayout.tabGravity = TabLayout.GRAVITY_FILL
            tabLayout.tabMode = TabLayout.MODE_FIXED

            var fragmentList: MutableList<Fragment> = ArrayList()
            var tabs: MutableList<String> =  ArrayList()
//            tabs.add(bean.title)

            for (bean in mList.segmentTitles){
                fragmentList.add(SonInsideFragment(mContext,mList?.controllers))
                tabs.add(bean)
            }

            viewPager.findViewById<ViewPager>(R.id.viewPager1).adapter = object : FragmentPagerAdapter(mFragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
                override fun getItem(position: Int): Fragment {
                    return fragmentList[position]
                }
                override fun getCount(): Int {
                    return fragmentList.size
                }
                override fun getPageTitle(position: Int): CharSequence? {
                    return tabs[position]
                }
            }
            //设置TabLayout和ViewPager联动
            tabLayout.setupWithViewPager(viewPager, false)

        }

    }
}