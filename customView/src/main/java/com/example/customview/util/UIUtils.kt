package com.example.customview.util

import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.ActivityInfo
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.viewpager.widget.ViewPager
import com.example.customview.R
import com.example.customview.bean.ImageBean
import com.example.customview.bean.SegmentDataBean
import com.example.customview.bean.TextBean
import com.example.customview.bean.ViewDataBean
import com.google.android.material.tabs.TabLayout

/**
 * Created by Android Studio.
 * User: wsl
 * Date: 7/15/21
 * Time: 12:23 PM
 * Description:
 */
class UIUtils {
    companion object instance {

         //宽高 包裹
        fun setWplayoutParams():RelativeLayout.LayoutParams{
            return RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.WRAP_CONTENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT)
        }
        //宽高 填充
        fun setMatlayoutParams(): RelativeLayout.LayoutParams{
            return RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.MATCH_PARENT)
        }

        //宽高 填充
        fun setCoordinatorParams(): CoordinatorLayout.LayoutParams{
            return CoordinatorLayout.LayoutParams(
                    CoordinatorLayout.LayoutParams.MATCH_PARENT,
                    CoordinatorLayout.LayoutParams.MATCH_PARENT)
        }

        fun setCoordinatorLayout(context: Context):CoordinatorLayout{
            var coordinator = CoordinatorLayout(context)
            coordinator.layoutParams = ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)

//            coordinator(LinearLayout.VERTICAL);
//            coordinator.orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
            return coordinator
        }


        @SuppressLint("WrongConstant")
        fun setConstraintLayout(context: Context): LinearLayout? {
            val constraintLayout = LinearLayout(context)

//        constraintLayout.id = R.id.clRoot
            constraintLayout.layoutParams = ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
            )
            constraintLayout.orientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
            //先设置根布局
//            setContentView(constraintLayout)

            return constraintLayout
        }
        /**
         * 添加textView
         */
        @SuppressLint("NewApi")
        fun addTextView(context: Context, textbean: TextBean):View{
            val textview =  TextView(context)
            textview.id = textbean.id
            textview.text = textbean.text
            textview.textSize = textbean.fontSize.toFloat()

            if(textbean.publicAtt.cornerRadius !=0){
                val gradientDrawable = GradientDrawable()
                gradientDrawable.shape = GradientDrawable.RECTANGLE
                gradientDrawable.setColor(Color.parseColor(textbean.publicAtt.backgroundColor))
                gradientDrawable.setStroke(0, Color.BLUE)
                gradientDrawable.cornerRadius = textbean.publicAtt.cornerRadius.toFloat()
                gradientDrawable.setSize(textbean.layout.width, textbean.layout.height)
                textview.background = gradientDrawable
            }
//            else{
//                textview.setBackgroundColor(Color.parseColor(textbean.publicAtt.backgroundColor))
//            }
            textview.setTextColor(Color.parseColor(textbean.textColor)) ;
//            val tvRightLayoutParams =  RelativeLayout.LayoutParams(
//                    DrawableUtil.dp2px(context, textbean.layout.width.toFloat()),
//                    DrawableUtil.dp2px(context, textbean.layout.height.toFloat()))
            val tvRightLayoutParams =   RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.WRAP_CONTENT,   RelativeLayout.LayoutParams.WRAP_CONTENT)

            margin(textview,  DrawableUtil.dp2px(context,textbean.layout.left.toFloat()), textbean.layout.top, textbean.layout.right, textbean.layout.bottom)
            tvRightLayoutParams.leftMargin = DrawableUtil.dp2px(context,textbean.layout.left.toFloat())
            tvRightLayoutParams.topMargin =  DrawableUtil.dp2px(context,textbean.layout.top.toFloat())
            if(textbean.aligned == "center"){
                textview.gravity = Gravity.CENTER//文字居中
            }else  if(textbean.aligned == "left"){
                textview.gravity = Gravity.LEFT//左边
            }else{
                textview.gravity = Gravity.RIGHT
            }
            textview.layoutParams = tvRightLayoutParams
            return textview
        }


        /**
         * 填充imgVIew
         */
        @SuppressLint("NewApi")
        fun addImgView(context: Context, imageBean: ImageBean) : ImageView {
            val constraintSet =   RelativeLayout.LayoutParams(
                    DrawableUtil.dp2px(context, imageBean.layout.width.toFloat()), DrawableUtil.dp2px(context, imageBean.layout.height.toFloat()))
            val ivLeft = ImageView(context)
            ivLeft.id =imageBean.id
//            ivLeft.scaleType = ImageView.ScaleType.CENTER_CROP
            ivLeft.setImageResource(R.drawable.icon_about_maoti)
            margin(ivLeft, imageBean.layout.left, imageBean.layout.top, imageBean.layout.right, imageBean.layout.bottom)
            constraintSet.topMargin =  DrawableUtil.dp2px(context,imageBean.layout.top.toFloat())
            ivLeft.layoutParams = constraintSet

            return  ivLeft

        }

        /**
         * 填充Button
         */
        @SuppressLint("NewApi")
        fun addButtonView(context: Context, imageBean: ImageBean) : Button {
            val constraintSet =   RelativeLayout.LayoutParams(
                    DrawableUtil.dp2px(context, imageBean.layout.width.toFloat()), DrawableUtil.dp2px(context, imageBean.layout.height.toFloat()))
            val mButton = Button(context)
//            ivLeft.id =imageBean.id
//            ivLeft.scaleType = ImageView.ScaleType.CENTER_CROP
            margin(mButton, imageBean.layout.left, imageBean.layout.top, imageBean.layout.right, imageBean.layout.bottom)
            constraintSet.topMargin = imageBean.layout.top
            mButton.layoutParams = constraintSet

            return  mButton

        }


        /**
         * 添加View填充
         */
        fun addVIew(context: Context,viewData:ViewDataBean):View{
//            val layoutparams =   RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT)
            val layoutparams =   RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT, DrawableUtil.dp2px(context, viewData.layout.height.toFloat()))
            val view = View(context)

            if(viewData.publicAtt.cornerRadius !=0){
                val gradientDrawable = GradientDrawable()
                gradientDrawable.shape = GradientDrawable.RECTANGLE
                gradientDrawable.setColor(Color.parseColor(viewData.publicAtt.backgroundColor))
//                gradientDrawable.setStroke(0, Color.BLUE)
                gradientDrawable.cornerRadius = viewData.publicAtt.cornerRadius.toFloat()
//                gradientDrawable.setSize(textbean.layout.width, textbean.layout.height)
                view.background = gradientDrawable
            }
//            margin(view, viewData.layout.left, viewData.layout.top, viewData.layout.right, viewData.layout.bottom)
//            margin(view,  DrawableUtil.dp2px(context,viewData.layout.left.toFloat()), DrawableUtil.dp2px(context,viewData.layout.top.toFloat()),
//                    viewData.layout.right, viewData.layout.bottom)
            layoutparams.topMargin = DrawableUtil.dp2px(context,viewData.layout.top.toFloat())
            view.layoutParams = layoutparams
            return view
        }

        /**
         * 添加TabLayout
         */
        @SuppressLint("ResourceType")
        fun addTabLayout(context: Context, height:Float, segmented: SegmentDataBean.Segmented):TabLayout{
            val mTabLayout = TabLayout(context)
            val layoutparams =   RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,   RelativeLayout.LayoutParams.WRAP_CONTENT)

            if(height.toInt() !=0){
                layoutparams.topMargin =  DrawableUtil.dp2px(context,height)
            }
            mTabLayout.setTabTextColors(Color.parseColor(segmented.titleNormalColor),Color.parseColor(segmented.titleSelectedColor))
//            mTabLayout.setTab
            //设置tab 未选中/选中 文字颜色
//            mTabLayout.setTabTextColors(context.resources.getColor( Color.parseColor(segmented.titleSelectedColor)),
//                                        context.resources.getColor( Color.parseColor(segmented.titleNormalColor)))
//            mTabLayout.setselectedi
//        //设置tab未选中/选中 文字大小
//            mTabLayout.setTabTextSize(DrawableUtil.dp2px(context,30f), DrawableUtil.dp2px(context,30f));
//            //设置tab文字选中时加粗
//            mTabLayout.setTextSelectedBold(true);
//            //设置指示器宽度
//            mTabLayout.setTabIndicatorWidth(DTLUtils.getPixels(40));
//            //设置指示器高度
//            mTabLayout.setSelectedIndicatorHeight(DTLUtils.getPixels(6));


            mTabLayout.layoutParams = layoutparams
            return  mTabLayout
        }

        /**
         * 添加ViewPager
         */
        fun addViewPager(context: Context,id:Int):ViewPager{
            val mViewPageer = ViewPager(context)
            val layoutparams =   RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT,   RelativeLayout.LayoutParams.MATCH_PARENT)
            mViewPageer.id = id
            mViewPageer.layoutParams = layoutparams
            return mViewPageer
        }

        fun margin(v: View, l: Int, t: Int, r: Int, b: Int) {
            if (v.layoutParams is ViewGroup.MarginLayoutParams) {
                val p: ViewGroup.MarginLayoutParams = v.layoutParams as ViewGroup.MarginLayoutParams
                p.setMargins(l, t, r, b)
                v.requestLayout()
            }
        }

        fun setLayoutParams(context: Context, l: Int, t: Int, r: Int, b: Int): CoordinatorLayout.LayoutParams{
            val lp2 = CoordinatorLayout.LayoutParams(CoordinatorLayout.LayoutParams.WRAP_CONTENT, CoordinatorLayout.LayoutParams.WRAP_CONTENT)
            lp2.setMargins(DrawableUtil.dp2px(context, l.toFloat()), DrawableUtil.dp2px(context, t.toFloat()),
                    DrawableUtil.dp2px(context, r.toFloat()), DrawableUtil.dp2px(context, b.toFloat()))
            return  lp2
        }

    }


}