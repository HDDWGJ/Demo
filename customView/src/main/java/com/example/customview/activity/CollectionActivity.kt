package com.example.customview.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.customview.R
import com.example.customview.bean.CollectionBean
import com.example.customview.bean.TableBean
import com.example.customview.util.JsonUtil
import com.google.gson.Gson

class CollectionActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_collection)
        getGeoJson()
    }

    fun getGeoJson(){
        var path = "GGUICollectionData.geojson";
        var stringJson =  JsonUtil.getJson(applicationContext,path)
        var tabBean  =    Gson().fromJson<CollectionBean>(stringJson, CollectionBean::class.java)
        Log.i("tabString",tabBean.toString())
//        JavaBean javabean=gson.formJson(jsonData,JavaBean.class)
    }
}