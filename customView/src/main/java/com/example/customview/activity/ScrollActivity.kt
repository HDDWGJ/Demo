package com.example.customview.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.customview.R
import com.example.customview.bean.ScrollBean
import com.example.customview.bean.TableBean
import com.example.customview.util.JsonUtil
import com.google.gson.Gson

class ScrollActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scroll)
        getGeoJson()
    }

    fun getGeoJson(){
        var path = "GGUIScrollData.geojson";
        var stringJson =  JsonUtil.getJson(applicationContext,path)
        var tabBean  =    Gson().fromJson<ScrollBean>(stringJson, ScrollBean::class.java)
        Log.i("tabString",tabBean.toString())
//        JavaBean javabean=gson.formJson(jsonData,JavaBean.class)
    }
}