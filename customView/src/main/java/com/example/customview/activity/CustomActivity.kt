package com.example.customview.activity

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintSet
import androidx.constraintlayout.widget.Guideline
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.customview.R
import com.example.customview.adapter.ItemPersonalAdapter
import com.example.customview.adapter.ListAdapter
import com.example.customview.bean.TableBean
import com.example.customview.util.JsonUtil
import com.example.customview.util.ScreenUtil
import com.example.customview.util.UIUtils
import com.google.gson.Gson


/**
 * 集合列表数据UI 绘制展示
 */
class CustomActivity : AppCompatActivity(),ItemPersonalAdapter.ListenerItemBack {

    var mItemAdapter : ItemPersonalAdapter? =null
    var mItemData: MutableList<TableBean> =ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_custom)
//        changeConstraintSet();
//        addGuideLineUseLayoutParams()
//        addTextView()

        addRecyclerView()

    }



    @SuppressLint("NewApi")
    fun  addRecyclerView(){
        var constart =  setConstraintLayout()
        val recyclerView = RecyclerView(this)

        recyclerView.id = R.id.recyclerView
        val tvRightLayoutParams = UIUtils.setMatlayoutParams()

        tvRightLayoutParams.marginStart =16// ScreenUtil.dpToPx(this, 16)
        tvRightLayoutParams.topMargin = 16//ScreenUtil.dpToPx(this, 16)

        recyclerView.layoutParams = tvRightLayoutParams
        constart.addView(recyclerView)
//        InitDataItem(recyclerView)
        InitAdapter(recyclerView)
    }

    fun setConstraintLayout(): RelativeLayout {
        val constraintLayout = RelativeLayout(this)

        constraintLayout.id = R.id.clRoot
        constraintLayout.layoutParams = ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
        )
        //先设置根布局
        setContentView(constraintLayout)

        return constraintLayout
    }

    fun InitAdapter(recycler :RecyclerView){
        var mList :  MutableList<String> =ArrayList()
        var bean =  getGeoJson()

//        mList = bean.listData
        var mListAdapter = ListAdapter(baseContext,bean.listData.items)
        val layoutManager = GridLayoutManager(this, 1)
        recycler.layoutParams = UIUtils.setMatlayoutParams()
        recycler.layoutManager = layoutManager
        recycler.adapter = mListAdapter

        for (index in 1..10){
            mList.add(index.toString())
        }
        mListAdapter.data = mList
        mListAdapter?.notifyDataSetChanged()
    }

    fun getGeoJson():TableBean{
        var path = "GGUITableData.geojson";
        var stringJson =  JsonUtil.getJson(applicationContext,path)
        var tabBean  =    Gson().fromJson<TableBean>(stringJson,TableBean::class.java)
        return tabBean
    }



    override fun ItembackPosition(position: Int) {
//       Toast.makeText(this,position,1)
    }


}