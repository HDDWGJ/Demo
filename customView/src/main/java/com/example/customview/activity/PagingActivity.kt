package com.example.customview.activity

import android.annotation.SuppressLint
import android.os.Bundle
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import com.example.customview.R
import com.example.customview.bean.JsonBean
import com.example.customview.util.JsonUtil
import com.example.customview.util.LayoutUtils
import com.example.customview.util.UIUtils
import com.google.gson.Gson


/**
 * 整个版面UI 绘制
 */
class PagingActivity : AppCompatActivity() {

    var constart : LinearLayout? = null
            override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_paging)

        addView()
    }

    fun getGeoJson(): JsonBean {
        var path = "GGUIPagingData.geojson";
        var stringJson =  JsonUtil.getJson(applicationContext, path)
        var tabBean  =    Gson().fromJson<JsonBean>(stringJson, JsonBean::class.java)
        return tabBean
    }


    @SuppressLint("NewApi")
    fun  addView(){
         constart =  UIUtils.setConstraintLayout(this)
        //先设置根布局
        setContentView(constart)
        var bean=    getGeoJson()
        when(bean.type){
            1 -> {
//                constart?.addView(LayoutUtils.addRecyclerView(this,bean.segmentData.list))
            }
            2 -> {

            }
            3 -> {
                constart?.addView(LayoutUtils.addFragmentView(bean.segmentData,this,supportFragmentManager))

//                addSonView(bean.segmentData)
            }
            4 -> {

            }
        }
    }




}