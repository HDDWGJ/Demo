package com.example.modulejsonui.activity

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View.OVER_SCROLL_NEVER
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.widget.NestedScrollView
import com.example.customview.bean.JsonBean
import com.example.customview.util.LayoutUtils
import com.example.modulejsonui.bean.UrlListBean
import com.example.modulejsonui.net.JsonNet
import com.example.modulejsonui.util.CommonJSONParserUtil
import com.example.modulejsonui.util.JsonsUtil
import com.example.modulejsonui.util.thread.OnUploadListener
import com.example.modulejsonui.util.thread.UploadUtil
import com.google.gson.Gson
import com.lamfire.json.JSON
import com.lamfire.json.JSONObject
import com.maoti.lib.net.ResponseResult
import com.maoti.lib.net.interceptor.DefaultObserver
import com.sowell.mvpbase.mvputils.StatusCompat
import com.sowell.mvpbase.presenter.BaseNullKtPresenter
import com.sowell.mvpbase.view.MvpBaseActivity
import java.util.*
import kotlin.collections.ArrayList

class IntegralJsonActivity : MvpBaseActivity<BaseNullKtPresenter>(){

    var mLinearLayout : LinearLayout? = null
    private var newAlpha: Float = 0F
    var scrollview : NestedScrollView? = null
    var myTitleLay : CoordinatorLayout? = null


    private var mUploadUtil: UploadUtil? = null
    private var isRunning = false //判断线程池是否运行 标志位


    var  JsonType = "GGIntegralData.geojson"
    var bean :JsonBean?=null
    val jsonMap: MutableMap<String, String> = HashMap()

    companion object{
        val type = "type"

        fun startActivity(ctx: Context, user_id: String) {
            val to = Intent(ctx, IntegralJsonActivity::class.java)
            to.putExtra(type, user_id)
            ctx.startActivity(to)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        StatusCompat.setStatusBarColor(this, Color.TRANSPARENT)
//        setContentView(R.layout.activity_json_integral)
        JsonType = this.intent.extras!!.getString(type, "")

         bean=   getGeoJson()
        addView()


        myTitleLay?.background?.alpha = 0
        scrollview?.setOnScrollChangeListener { v: NestedScrollView?, scrollX: Int, scrollY: Int, oldScrollX: Int, oldScrollY: Int ->
            val scale = scrollY / 375.00F
            newAlpha = (scale * 255).toFloat()
            if (newAlpha > 255) {
                newAlpha = 255.00F
            }
            myTitleLay?.background?.alpha = newAlpha.toInt()
        }
        setOnCLick()

    }

    /**
     * 动态请求后台服务接口
     */
    fun getJsonObject(netList: MutableList<UrlListBean>){


        mUploadUtil = UploadUtil(100)
        startUpload(netList) //开始上传

        mUploadUtil!!.setOnUploadListener(object : OnUploadListener {
            //结果回调 执行在UI线程
            override fun onAllSuccess() {
                isRunning = false
                Log.i("textGetJson", "全部上传成功")
                Log.i("textGetJson", jsonMap.toString())

                addView()
                setOnCLick()
            }
            override fun onAllFailed() {
                isRunning = false
                Log.i("textGetJson", "上传过程中断")
            }

            override fun onThreadProgressChange(position: Int, percent: Int) {
//                mTextArray.get(position).setText("文件" + position + "上传" + percent + "%")
            }

            override fun onThreadFinish(position: Int, objects: String) {
                jsonMap.put(netList[position].alias, JSON.toString(objects))
                Log.i("textGetJson", "文件" + position + "上传成功 "+netList[position].alias)
            }
            override fun onThreadInterrupted(position: Int) {
                Log.i("textGetJson", "文件" + position + "上传失败")
            }
        })

    }

    private fun startUpload(netList: MutableList<UrlListBean>) { //模拟上传文件
        isRunning = true
        val files : ArrayList<String>  = ArrayList()
        for (bean in netList){
            files.add(bean.url)
        }

        mUploadUtil!!.submitAll(files)
    }

    private fun getGeoJson(): JsonBean {
        var stringJson =  JsonsUtil.getJson(applicationContext, JsonType)
        var tabBean  =    Gson().fromJson<JsonBean>(stringJson, JsonBean::class.java)
        getJsonObject(tabBean.listData?.netList!!)
        return tabBean
    }


    @SuppressLint("NewApi")
    private fun  addView(){
        try {
            mLinearLayout?.removeAllViews()
            mLinearLayout =  LayoutUtils.setLinearLayout(this)//根布局
            setContentView(mLinearLayout) //置根布局

            when(bean?.type){
                1 -> {
                    if (bean?.listData != null) {
                        var mRelativeLayout = LayoutUtils.setRelativeLayout(this)
                        var linearLayout = LayoutUtils.setLinearLayout(this, LinearLayout.VERTICAL)
                        scrollview = LayoutUtils.setNestedScrollView(this)
                        scrollview?.overScrollMode = OVER_SCROLL_NEVER

                        myTitleLay = LayoutUtils.setTitleViewData(this, bean?.listData?.naviData!!)//设置标题
                        linearLayout?.addView(LayoutUtils.setPlateView(this, bean?.listData?.items!!,jsonMap))//设置内容
                        scrollview?.addView(linearLayout)

//                        scrollview?.isHorizontalFadingEdgeEnabled = false
                        mRelativeLayout.addView(scrollview)
                        mRelativeLayout.addView(myTitleLay)

                        mLinearLayout?.addView(mRelativeLayout)

                    }
                }
                2 -> {

                }
                3 -> {
                    mLinearLayout?.addView(LayoutUtils.addFragmentView(bean?.segmentData!!, this, supportFragmentManager))
                }
                4 -> {

                }
            }


        }catch (e: Exception){

        }
    }

    @SuppressLint("ResourceType")
    fun  setOnCLick(){
        mLinearLayout?.findViewById<ImageView>(105)?.setOnClickListener {
            finish()
        }
    }

}