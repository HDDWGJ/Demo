package com.example.modulejsonui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.example.modulejsonui.R

class DemoActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_demo)
        setOnc()
    }


    fun setOnc(){
        findViewById<Button>(R.id.button1).setOnClickListener {
            IntegralJsonActivity.startActivity(this,"GGIntegralData.geojson")
        }
        findViewById<Button>(R.id.button2).setOnClickListener {
            IntegralJsonActivity.startActivity(this,"GGUIPagingData.geojson")
        }
        findViewById<Button>(R.id.button3).setOnClickListener {

        }
        findViewById<Button>(R.id.button4).setOnClickListener {

        }
    }
}