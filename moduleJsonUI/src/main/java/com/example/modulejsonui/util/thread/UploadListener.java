package com.example.modulejsonui.util.thread;

import java.util.concurrent.CountDownLatch;

/**
 * Created by Android Studio.
 * User: wsl
 * Date: 2020/12/15
 * Time: 3:41 PM
 * Description: 主线程异步回调接口
 */
public class UploadListener implements Runnable {
    private CountDownLatch downLatch;
    private OnAllThreadResultListener listener;

    public UploadListener(CountDownLatch countDownLatch,OnAllThreadResultListener listener){
        this.downLatch=countDownLatch;
        this.listener=listener;
    }

    @Override
    public void run() {
        try {
            downLatch.await();
            listener.onSuccess();//顺利完成
        } catch (InterruptedException e) {
            listener.onFailed();
        }
    }
}