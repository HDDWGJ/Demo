package com.example.modulejsonui.util.thread;

/**
 * Created by Android Studio.
 * User: wsl
 * Date: 2020/12/15
 * Time: 3:40 PM
 * Description: 定义Runnable对象UploadFile的任务线程：
 */
public interface OnUploadListener {
    void onAllSuccess();
    void onAllFailed();
    void onThreadProgressChange(int position,int percent);
    void onThreadFinish(int position,String jsonObject);
    void onThreadInterrupted(int position);
}
