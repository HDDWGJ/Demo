package com.example.modulejsonui.util

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

/**
 * Created by Android Studio.
 * User: wsl
 * Date: 8/5/21
 * Time: 12:05 PM
 * Description:
 */
class CommonJSONParserUtil {
    companion object {
        fun parse(jsonStr: String?): Map<String?, Any?>? {
            var result: Map<String?, Any?>? = null
            if (null != jsonStr) {
                try {
                    val jsonObject = JSONObject(jsonStr)
                    result = parseJSONObject(jsonObject)
                } catch (e: JSONException) {
                    // TODO Auto-generated catch block
                    e.printStackTrace()
                }
            } // if (null != jsonStr)
            return result
        }





        private fun parseJSONObject(jsonObject: JSONObject?): Map<String?, Any?>? {
            var valueObject: MutableMap<String?, Any?>? = null
            if (null != jsonObject) {
                valueObject = HashMap()
                val keyIter = jsonObject.keys()
                while (keyIter.hasNext()) {
                    val keyStr = keyIter.next()
                    val itemObject = jsonObject.opt(keyStr)
                    if (null != itemObject) {
                        valueObject[keyStr] = parseValue(itemObject)
                    } // if (null != itemValueStr)
                } // while (keyIter.hasNext())
            } // if (null != valueStr)
            return valueObject
        }


        private fun parseValue(inputObject: Any?): Any? {
            var outputObject: Any? = null
            if (null != inputObject) {
                if (inputObject is JSONArray) {
                    outputObject = parseJSONArray(inputObject as JSONArray?)
                } else if (inputObject is JSONObject) {
                    outputObject = parseJSONObject(inputObject as JSONObject?)
                } else if (inputObject is String || inputObject is Boolean || inputObject is Int) {
                    outputObject = inputObject
                }
            }
            return outputObject
        }


        private fun parseJSONArray(jsonArray: JSONArray?): List<Any?>? {
            var valueList: MutableList<Any?>? = null
            if (null != jsonArray) {
                valueList = ArrayList()
                for (i in 0 until jsonArray.length()) {
                    val itemObject = jsonArray[i]
                    if (null != itemObject) {
                        valueList.add(parseValue(itemObject))
                    }
                } // for (int i = 0; i < jsonArray.length(); i++)
            } // if (null != valueStr)
            return valueList
        }


        /**
         *把字符str1中的str2替换为str3
         */
        fun RplStr(str1: String, str2: String, str3: String?): String? {
            var strtmp: String? = ""
            var i = 0
            var f: Int
            i = 0
            while (true) {
                f = str1.indexOf(str2, i)
                if (f == -1) {
                    strtmp += str1.substring(i)
                    break
                } else {
                    strtmp += str1.substring(i, f)
                    strtmp += str3
                    i = f
                }
                i += str2.length
            }
            return strtmp
        }


        /**
         * 去掉字符串头尾指定字符
         * @param begin    需要处理的字符串
         * @return
         */
        open fun removeQuotation(begin: String): String? {
            var result = ""
            val begins = begin.toCharArray()
            var k = 0
            var m = begins!!.size
            if (begins != null) {
                val first = begins[0]
                val last = begins[m - 1]
                if (first == '"') {
                    k = 1
                }
                if (last == '"') {
                    m = m - 1
                }
            }
            result = begin.substring(k, m)
            return result
        }
    }
}