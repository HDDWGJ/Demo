package com.example.modulejsonui.util.thread;

import com.lamfire.json.JSONObject;

/**
 * Created by Android Studio.
 * User: wsl
 * Date: 2020/12/15
 * Time: 3:40 PM
 * Description: 任务线程的回调接口：
 */
public interface OnThreadResultListener {
    void onProgressChange(int percent);//进度变化回调
    void onFinish(JSONObject jsonData);//线程完成时回调
    void onInterrupted();//线程被中断回调
}
