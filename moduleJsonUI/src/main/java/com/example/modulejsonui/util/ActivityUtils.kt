package com.example.modulejsonui.util

import android.app.Activity
import android.content.Context
import android.content.Intent
import org.json.JSONArray
import java.lang.reflect.Field
import java.lang.reflect.Method


/**
 * Created by Android Studio.
 * User: wsl
 * Date: 8/16/21
 * Time: 5:25 PM
 * Description:
 */
class ActivityUtils {
    companion object {

        /**
         * 打开指定Activity
         */
        fun  openActivity(context: Context, className: String){
//            val className: String = context.getPackageName().toString() + ".view.Activity" + (com.sowell.maoti.R2.id.position + 1)
//            val className: String ="com.sowell.maoti.ui.personal.wallet.WalletActivity"
                    try {
                val activityClass = Class.forName(className)
                context.startActivity(Intent(context, activityClass))
            } catch (e: ClassNotFoundException) {
                e.printStackTrace()
            }
        }




        /** 调用类静态方法  */
        fun CallStaticMethod(className: String, methodName: String, vararg param: Any?): Any? {
            var result: Any? = null
            var methodObj: Method? = null
            var classType: Class<*>? = null
            try {
                classType = Class.forName(className)
                methodObj = classType.getMethod(methodName) // 获取类中的函数方法
                methodObj.setAccessible(true)
                result = methodObj.invoke(null, param) // 反射调用函数方法classObj.methodName()
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
            checkNotNull(classType) { "Could not find Class $className" }
            checkNotNull(methodObj) { "Could not find Static method " + methodName + "() in the class " + classType.name }
            return result
        }

        /**
         * 反射调用，Obj对象的methodName()函数
         */
        fun CallMethod(Obj: Any?, methodName: String): Any? {
            if (Obj == null) throw NullPointerException("Obj == null")
            var result: Any? = null
            var methodObj: Method? = null
            var classType: Class<*>? = null
            try {
                classType = Obj.javaClass
                methodObj = classType.getMethod(methodName) // 获取类中的函数方法
                result = methodObj.invoke(Obj) // 反射调用函数方法classObj.methodName()
            } catch (ex: java.lang.Exception) {
                ex.printStackTrace()
            }
            checkNotNull(methodObj) { "Could not find a method " + methodName + "() in the class " + classType!!.name }
            return result
        }

        /** 反射调用函数methodName(argClass, argObj)  */
        fun CallMethod(Obj: Any?, methodName: String, argClass: Class<*>, argObj: Any?): Any? {
            if (Obj == null) throw java.lang.NullPointerException("Obj == null")
            var result: Any? = null
            var methodObj: Method? = null
            var classType: Class<*>? = null
            try {
                classType = Obj.javaClass
                methodObj = classType.getMethod(methodName, argClass) // 获取类中的函数方法
                result = methodObj.invoke(Obj, argObj) // 反射调用函数方法classObj.methodName(argObj)
            } catch (ex: java.lang.Exception) {
                ex.printStackTrace()
            }
            checkNotNull(methodObj) { "Could not find a method " + methodName + "(" + argClass.simpleName + ") in the class " + classType!!.name }
            return result
        }



    }
}