package com.example.customview.util

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.customview.bean.ImageBean
import com.example.customview.bean.SegmentDataBean
import com.example.customview.bean.TextBean
import com.example.customview.bean.ViewDataBean
import com.example.modulejsonui.bean.ActionBean
import com.example.modulejsonui.bean.ButtonBean
import com.example.modulejsonui.util.ActivityUtils
import com.google.android.material.tabs.TabLayout
import com.maoti.ad.bean.AdvertisingBean
import com.zhouwei.mzbanner.MZBannerView


/**
 * Created by Android Studio.
 * User: wsl
 * Date: 7/15/21
 * Time: 12:23 PM
 * Description: 单独view 生成 返回
 */
class ViewAddUtils {
    companion object instance {

         //宽高 包裹
        fun setWplayoutParams():RelativeLayout.LayoutParams{
            return RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.WRAP_CONTENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT)
        }


        //宽高 填充
        fun setCoordinatorParams(): CoordinatorLayout.LayoutParams{
            return CoordinatorLayout.LayoutParams(
                    CoordinatorLayout.LayoutParams.MATCH_PARENT,
                    CoordinatorLayout.LayoutParams.MATCH_PARENT)
        }

        fun setCoordinatorLayout(context: Context):CoordinatorLayout{
            var coordinator = CoordinatorLayout(context)
            coordinator.layoutParams = ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            return coordinator
        }


        /**
         * 添加textView
         */
        @SuppressLint("NewApi")
        fun addTextView(context: Context, textbean: TextBean):View{
            val textview =  TextView(context)
            textview.id = textbean.id
            textview.text = textbean.text


            textview.textSize = textbean.fontSize.toFloat()
//            textview.textSize =  DrawableUtil.sp2px(context,textbean.fontSize).toFloat()
//            textview.width = 1

            if(textbean.publicAtt != null && textbean.publicAtt.cornerRadius !=0){
                val gradientDrawable = GradientDrawable()
                gradientDrawable.shape = GradientDrawable.RECTANGLE
                gradientDrawable.setColor(Color.parseColor(textbean.publicAtt.backgroundColor))
                gradientDrawable.setStroke(0, Color.BLUE)
                gradientDrawable.cornerRadius = textbean.publicAtt.cornerRadius!!.toFloat()
                gradientDrawable.setSize(textbean.layout.width, textbean.layout.height)
                textview.background = gradientDrawable
            }
            textview.setTextColor(Color.parseColor(textbean.textColor))
            var tvRightLayoutParams : RelativeLayout.LayoutParams?= null
            if(textbean.layout.width !=0&& textbean.layout.width !=0){
                tvRightLayoutParams =   RelativeLayout.LayoutParams(
                        DrawableUtil.dp2px(context, textbean.layout.width.toFloat()),
                        DrawableUtil.dp2px(context, textbean.layout.height.toFloat()))
            }else{
                tvRightLayoutParams =   RelativeLayout.LayoutParams(
                        RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT)
            }


            textview.setPadding(DrawableUtil.dp2px(context, textbean.layout.left.toFloat()), DrawableUtil.dp2px(context, textbean.layout.top.toFloat()),
                    DrawableUtil.dp2px(context, textbean.layout.right.toFloat()), DrawableUtil.dp2px(context, textbean.layout.bottom.toFloat()))
            tvRightLayoutParams?.leftMargin = DrawableUtil.dp2px(context, textbean.layout.left.toFloat())
            tvRightLayoutParams?.topMargin =  DrawableUtil.dp2px(context, textbean.layout.top.toFloat())

            textview.setOnClickListener(View.OnClickListener { view ->

                setOnClick(context, view, textview.text.toString(),textbean.action)
            })
            if(textbean.aligned == "left"){
                textview.gravity = Gravity.LEFT//左边
            }else  if(textbean.aligned == "right"){
                textview.gravity = Gravity.RIGHT
            }else{
                textview.gravity = Gravity.CENTER//文字居中
            }
            textview.layoutParams = tvRightLayoutParams
            return textview
        }


        /**
         * 填充imgVIew
         */
        @SuppressLint("NewApi")
        fun addImgView(context: Context, imageBean: ImageBean) : ImageView {
            var  width  = RelativeLayout.LayoutParams.MATCH_PARENT
            var height  = RelativeLayout.LayoutParams.WRAP_CONTENT
            if(imageBean.layout.width != 0){
                width =  DrawableUtil.dp2px(context, imageBean.layout.width.toFloat())
            }
            if(imageBean.layout.height !=0){
               height =  DrawableUtil.dp2px(context, imageBean.layout.height.toFloat())
            }
            val constraintSet =   RelativeLayout.LayoutParams(width, height)

            val image = ImageView(context)
            image.id =imageBean.id
//            ivLeft.scaleType = ImageView.ScaleType.CENTER_CROP
            var resID = context.resources.getIdentifier(imageBean.localImgae, "mipmap", context.packageName)
            image.setImageResource(resID)

//            Glide.with(context).load(imageBean.localImgae).into(image)// 加载图片

            image.setOnClickListener(View.OnClickListener { view ->
                setOnClick(context, view, "图片",imageBean.action)
            })
//            GlideImageLoader.
//            ImageLoaderHelper.loadImageByGlide(headimg, newInfo.getImgs().get(0).getPath(), R.mipmap.shouye_img_xizun4_zanweifu, null)
            image.setPadding(DrawableUtil.dp2px(context, imageBean.layout.left.toFloat()), DrawableUtil.dp2px(context, imageBean.layout.top.toFloat()),
                    DrawableUtil.dp2px(context, imageBean.layout.right.toFloat()), DrawableUtil.dp2px(context, imageBean.layout.bottom.toFloat()))
            constraintSet.topMargin =  DrawableUtil.dp2px(context, imageBean.layout.top.toFloat())
            constraintSet.leftMargin = DrawableUtil.dp2px(context, imageBean.layout.left.toFloat())
            image.layoutParams = constraintSet

            return  image

        }

        /**
         * 填充Button
         */
        @SuppressLint("NewApi")
        fun addButtonView(context: Context, buttonBean: ButtonBean) : Button {
            var  width  = RelativeLayout.LayoutParams.MATCH_PARENT
            var height  = RelativeLayout.LayoutParams.WRAP_CONTENT
            if(buttonBean.layout.width != 0){
                width =  DrawableUtil.dp2px(context, buttonBean.layout.width.toFloat())
            }
            if(buttonBean.layout.height !=0){
                height =  DrawableUtil.dp2px(context, buttonBean.layout.height.toFloat())
            }
            val constraintSet =   RelativeLayout.LayoutParams(width, height)
            val mButton = Button(context)

            if(buttonBean.publicAtt != null && buttonBean.publicAtt.cornerRadius !=0){
                val gradientDrawable = GradientDrawable()
                gradientDrawable.shape = GradientDrawable.RECTANGLE
                gradientDrawable.setColor(Color.parseColor(buttonBean.publicAtt.backgroundColor))
                buttonBean.publicAtt.borderWidth?.let { gradientDrawable.setStroke(it, Color.parseColor(buttonBean.publicAtt.borderColor)) }

                gradientDrawable.cornerRadius = DrawableUtil.dp2px(context, buttonBean.publicAtt.cornerRadius!!.toFloat()).toFloat()
                gradientDrawable.setSize(buttonBean.layout.width, buttonBean.layout.height)
                mButton.background = gradientDrawable
            }

            if(buttonBean.localImgae != null){
                var resID = context.resources.getIdentifier(buttonBean.localImgae!!, "mipmap", context.packageName)
                var drawable=context.resources.getDrawable(resID)
//                drawable.setBounds(0, 0, 0, 2)
                mButton.setPadding(0, DrawableUtil.dp2px(context, 5f), 0, 0)
                if(buttonBean.imageStyle == "left"){
                    mButton.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null)
                }else if(buttonBean.imageStyle == "top"){
                    mButton.setCompoundDrawablesWithIntrinsicBounds(null, drawable, null, null)
                }else if(buttonBean.imageStyle == "right"){
                    mButton.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null)
                }else if(buttonBean.imageStyle == "bottom"){
                    mButton.setCompoundDrawablesWithIntrinsicBounds(null, null, null, drawable)
                }


            }else{
                mButton.setPadding(0, 0, 0, 0)
            }

            mButton.compoundDrawablePadding = 2
//            image.setImageResource(resID)
            mButton.text = buttonBean.text
            mButton.textSize = buttonBean.fontSize
            mButton.gravity = Gravity.CENTER
            mButton.setTextColor(Color.parseColor(buttonBean.textColor))


            mButton.setOnClickListener(View.OnClickListener { view ->
                setOnClick(context, view, mButton.text.toString(),buttonBean.action)
            })
//            ivLeft.id =imageBean.id
//            ivLeft.scaleType = ImageView.ScaleType.CENTER_CROP
//            margin(mButton, buttonBean.layout.left, buttonBean.layout.top, buttonBean.layout.right, buttonBean.layout.bottom)

            constraintSet.topMargin =  DrawableUtil.dp2px(context, buttonBean.layout.top.toFloat())
            constraintSet.leftMargin = DrawableUtil.dp2px(context, buttonBean.layout.left.toFloat())
            constraintSet.rightMargin = DrawableUtil.dp2px(context, buttonBean.layout.right.toFloat())
            mButton.layoutParams = constraintSet
            return  mButton

        }

        /**
         * 添加广告view
         */
        fun addMZBannerView(context: Context, listbean: MutableList<AdvertisingBean>):MZBannerView<AdvertisingBean>{
            val mMzView  = MZBannerView<AdvertisingBean>(context)
            mMzView.top = 13
//            mMzView.setPages(listbean, MZHolderCreator { BannerPaddingViewHolder() } as MZHolderCreator<BannerPaddingViewHolder>)
//            margin(mMzView, imageBean.layout.left, imageBean.layout.top, imageBean.layout.right, imageBean.layout.bottom)
            mMzView.indicatorContainer
            return mMzView
        }


        /**
         * 添加View填充
         */
        fun addVIew(context: Context, viewData: ViewDataBean):View{
//            val layoutparams =   RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT)
            val layoutparams =   RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT, DrawableUtil.dp2px(context, viewData.layout.height.toFloat()))
            val view = View(context)

            if(viewData.publicAtt.cornerRadius !=null && viewData.publicAtt.cornerRadius !=0){
                val gradientDrawable = GradientDrawable()
                gradientDrawable.shape = GradientDrawable.RECTANGLE
                gradientDrawable.setColor(Color.parseColor(viewData.publicAtt.backgroundColor))
//                gradientDrawable.setStroke(0, Color.BLUE)
                gradientDrawable.cornerRadius = viewData.publicAtt.cornerRadius!!.toFloat()
//                gradientDrawable.setSize(textbean.layout.width, textbean.layout.height)
                view.background = gradientDrawable
            }
//            margin(view, viewData.layout.left, viewData.layout.top, viewData.layout.right, viewData.layout.bottom)
//            margin(view,  DrawableUtil.dp2px(context,viewData.layout.left.toFloat()), DrawableUtil.dp2px(context,viewData.layout.top.toFloat()),
//                    viewData.layout.right, viewData.layout.bottom)
            layoutparams.topMargin = DrawableUtil.dp2px(context, viewData.layout.top.toFloat())
            view.layoutParams = layoutparams


//            when (viewData.subItem.type){
//
//            }

            return view
        }

        /**
         * 添加TabLayout
         */
        @SuppressLint("ResourceType")
        fun addTabLayout(context: Context, height: Float, segmented: SegmentDataBean.Segmented):TabLayout{
            val mTabLayout = TabLayout(context)
            val layoutparams =   RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT)

            if(height.toInt() !=0){
                layoutparams.topMargin =  DrawableUtil.dp2px(context, height)
            }
            mTabLayout.setTabTextColors(Color.parseColor(segmented.titleNormalColor), Color.parseColor(segmented.titleSelectedColor))
//            mTabLayout.setTab
            //设置tab 未选中/选中 文字颜色
//            mTabLayout.setTabTextColors(context.resources.getColor( Color.parseColor(segmented.titleSelectedColor)),
//                                        context.resources.getColor( Color.parseColor(segmented.titleNormalColor)))
//            mTabLayout.setselectedi
//        //设置tab未选中/选中 文字大小
//            mTabLayout.setTabTextSize(DrawableUtil.dp2px(context,30f), DrawableUtil.dp2px(context,30f));
//            //设置tab文字选中时加粗
//            mTabLayout.setTextSelectedBold(true);
//            //设置指示器宽度
//            mTabLayout.setTabIndicatorWidth(DTLUtils.getPixels(40));
//            //设置指示器高度
//            mTabLayout.setSelectedIndicatorHeight(DTLUtils.getPixels(6));


            mTabLayout.layoutParams = layoutparams
            return  mTabLayout
        }


        fun  setOnClick(context: Context, view: View, str: String,action: ActionBean){
            if (view == null) {
                return
            }
            if(action.type ==1){

            }else if(action.type == 2){
                ActivityUtils.openActivity(context,action.controller)
            }
//            Toast.makeText(context, str, Toast.LENGTH_SHORT).show()
        }

        /**
         * 添加ViewPager
         */
        fun addViewPager(context: Context, id: Int):ViewPager{
            val mViewPageer = ViewPager(context)
            val layoutparams =   RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT)
            mViewPageer.id = id
            mViewPageer.layoutParams = layoutparams
            return mViewPageer
        }

        fun margin(v: View, l: Int, t: Int, r: Int, b: Int) {
            if (v.layoutParams is ViewGroup.MarginLayoutParams) {
                val p: ViewGroup.MarginLayoutParams = v.layoutParams as ViewGroup.MarginLayoutParams
                p.setMargins(l, t, r, b)
                v.requestLayout()
            }
        }

        fun setLayoutParams(context: Context, l: Int, t: Int, r: Int, b: Int): CoordinatorLayout.LayoutParams{
            val lp2 = CoordinatorLayout.LayoutParams(CoordinatorLayout.LayoutParams.WRAP_CONTENT, CoordinatorLayout.LayoutParams.WRAP_CONTENT)
            lp2.setMargins(DrawableUtil.dp2px(context, l.toFloat()), DrawableUtil.dp2px(context, t.toFloat()),
                    DrawableUtil.dp2px(context, r.toFloat()), DrawableUtil.dp2px(context, b.toFloat()))
            return  lp2
        }




    }


}