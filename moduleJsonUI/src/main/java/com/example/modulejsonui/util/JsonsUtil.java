package com.example.modulejsonui.util;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Android Studio.
 * User: wsl
 * Date: 7/14/21
 * Time: 3:15 PM
 * Description:
 */
public class JsonsUtil {
    /**
     * 从asset路径下读取对应文件转String输出
     * @param mContext
     * @return
     */
    public static String getJson(Context mContext, String fileName) {
        // TODO Auto-generated method stub
        StringBuilder sb = new StringBuilder();
        AssetManager am = mContext.getAssets();
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(am.open(fileName)));
            String next = "";
            while (null != (next = br.readLine())) {
                sb.append(next);
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            sb.delete(0, sb.length());
        }
        return sb.toString().trim();
    }

//    public static boolean iteraJsonOrArray(String source, Map map){
//        if(source.indexOf(":") == -1){
//            return true;
//        }
//        JSONObject fromObject = JSONObject.fromObject(source);
//        Iterator keys = fromObject.keys();
//        while(keys.hasNext()){
//            String key = keys.next().toString();
//            Object value = fromObject.get(key);
//            String val = value.toString();
//            if(val.indexOf("[{") == -1){
//                //说明不存在数组json即格式为："[{" 开头的数据。可以允许是[10,11,12]的非json数组
//                if(val.indexOf(":") == -1){
//                    map.put(key, val);
//                }else{
////                    iteraJson(val,map);
//                }
//            }else if(val.indexOf("[{") != -1){
//                //说明存在数组json即格式为：[{开头的json数组
//                if(val.indexOf("[{") == 0){
//                    //说明当前value就是一个json数组
//                    //去除[括号
//                    String jsons = val.substring(1, val.lastIndexOf("]"));//得到数据格式为：{...},{...},{...}
//                    //把上面得到jsons分割成数组
//                    //因为数据格式为{name:joker,age:20},{...},{...}，所以不能用逗号分割。否则会变"{name:joker" "age:20}"
//                    //使用正则表达式把},{替换成}|{
//                    jsons = jsons.replaceAll("\\}\\s?,\\s?\\{", "}|{");
//                    String[] split = jsons.split("\\|");
//                    for(int i = 0; i < split.length;i++){
//                        iteraJsonOrArray(split[i],map);//符合当前递归条件
//                    }
//
//                }else{
//                    //说明value可能是一个json，这个json中任然包含数组。例如：{inner:[{a:1,b:2,c:3}]}
//                    iteraJsonOrArray(val,map);//符合当前递归条件
//                }
//            }
//
//        }
//
//        return false;
//    }

}
