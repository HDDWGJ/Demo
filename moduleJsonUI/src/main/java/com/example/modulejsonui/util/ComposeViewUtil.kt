package com.example.modulejsonui.util

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.view.get
import com.example.customview.bean.ViewDataBean
import com.example.customview.util.DrawableUtil
import com.example.customview.util.LayoutUtils
import com.example.customview.util.ViewAddUtils


/**
 * Created by Android Studio.
 * User: wsl
 * Date: 7/28/21
 * Time: 5:44 PM
 * Description: View 组合组件返回
 */
class ComposeViewUtil {
    companion object instance {

        /**
         *设置模块
         */
        fun getIntegralTopView(context: Context, viewData: ViewDataBean, dataString: String): CoordinatorLayout {
            var mCoordinatorLayout = CoordinatorLayout(context)

            val layoutparams =   RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT, DrawableUtil.dp2px(context, viewData.layout.height.toFloat()))
//            val view = View(context)

            if(viewData.publicAtt.cornerRadius !=0){
                val gradientDrawable = GradientDrawable()
                gradientDrawable.shape = GradientDrawable.RECTANGLE
                gradientDrawable.setColor(Color.parseColor(viewData.publicAtt.backgroundColor))
//                gradientDrawable.setStroke(0, Color.BLUE)
                gradientDrawable.cornerRadius = viewData.publicAtt.cornerRadius!!.toFloat()
//                gradientDrawable.setSize(textbean.layout.width, textbean.layout.height)
                mCoordinatorLayout.background = gradientDrawable
            }
//            margin(view, viewData.layout.left, viewData.layout.top, viewData.layout.right, viewData.layout.bottom)
//            margin(view,  DrawableUtil.dp2px(context,viewData.layout.left.toFloat()), DrawableUtil.dp2px(context,viewData.layout.top.toFloat()),
//                    viewData.layout.right, viewData.layout.bottom)
            layoutparams.topMargin = DrawableUtil.dp2px(context, viewData.layout.top.toFloat())
//            layoutparams.leftMargin = DrawableUtil.dp2px(context,viewData.layout.left.toFloat())

            for ( item in viewData.subItem){
                var map = CommonJSONParserUtil.parse(
                        CommonJSONParserUtil.removeQuotation(dataString.replace("\\", "")))
                when(item.type){
                    0 -> {
                        if (item.labelData.textjsonData != null) {
                            item.labelData.text = map!![item.labelData.textjsonData].toString()
                            mCoordinatorLayout?.addView(ViewAddUtils.addTextView(context, item.labelData))
                        } else {
                            mCoordinatorLayout?.addView(ViewAddUtils.addTextView(context, item.labelData))
                        }
                    }
                    1 -> {
                        mCoordinatorLayout?.addView(ViewAddUtils.addButtonView(context, item.buttonData)) //这里赋值还需要改动
                    }
                    2 -> {
//                        if(item.imageData.layout.isCenterXRelative){
//                            layoutparams.addRule(RelativeLayout.CENTER_HORIZONTAL)// 设置文字居中
//                            mCoordinatorLayout?.addView(ViewAddUtils.addImgView(context, item.imageData),layoutparams)
//                        }else{
                        mCoordinatorLayout?.addView(ViewAddUtils.addImgView(context, item.imageData))
//                        }

                    }
                    3 -> {
                        mCoordinatorLayout?.addView(setIntegralSonTopView(context, item.viewData, dataString))
//                        mCoordinatorLayout?.addView(ViewAddUtils.addVIew(context,item.viewData))
                    }
                    4 -> {

                    }
                }
            }
            mCoordinatorLayout.layoutParams = layoutparams

            return mCoordinatorLayout
        }



        /**
         * 设置子模块
        */
        private fun setIntegralSonTopView(context: Context, viewData: ViewDataBean, dataString: String): CoordinatorLayout {
            var mCoordinatorLayout = CoordinatorLayout(context)

            val layoutparams =   RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT, DrawableUtil.dp2px(context, viewData.layout.height.toFloat()))

            if(viewData.publicAtt.cornerRadius !=0){
                val gradientDrawable = GradientDrawable()
                gradientDrawable.shape = GradientDrawable.RECTANGLE
                gradientDrawable.setColor(Color.parseColor(viewData.publicAtt.backgroundColor))
                gradientDrawable.cornerRadius = viewData.publicAtt.cornerRadius!!.toFloat()
                mCoordinatorLayout.background = gradientDrawable
            }

            layoutparams.topMargin = DrawableUtil.dp2px(context, viewData.layout.top.toFloat())
            layoutparams.leftMargin = DrawableUtil.dp2px(context, viewData.layout.left.toFloat())
            layoutparams.rightMargin = DrawableUtil.dp2px(context, viewData.layout.right.toFloat())

            for ( item in viewData.subItem){

                var map = CommonJSONParserUtil.parse(
                        CommonJSONParserUtil.removeQuotation(dataString.replace("\\", "")))
                when(item.type){
                    0 -> {
                        if (item.labelData.textjsonData != null) {
                            item.labelData.text = map!![item.labelData.textjsonData].toString()
                            mCoordinatorLayout?.addView(ViewAddUtils.addTextView(context, item.labelData))
                        } else {
                            mCoordinatorLayout?.addView(ViewAddUtils.addTextView(context, item.labelData))
                        }

                    }
                    1 -> {
                        mCoordinatorLayout?.addView(ViewAddUtils.addButtonView(context, item.buttonData))
                    }
                    2 -> {
                        mCoordinatorLayout?.addView(ViewAddUtils.addImgView(context, item.imageData))
                    }
                    3 -> {
                        mCoordinatorLayout?.addView(setIntegralSonTopView(context, item.viewData, dataString))
                    }
                    4 -> {

                        mCoordinatorLayout?.addView(LayoutUtils.addAdpterView(context, item.tableData.items[0].viewData, dataString))
//                        mCoordinatorLayout?.addView (LayoutUtils.setAdpaterSonTopView(context,item.tableData))
                    }
                }
            }

            mCoordinatorLayout.layoutParams = layoutparams

            return mCoordinatorLayout
        }


    }

}