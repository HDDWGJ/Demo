package com.example.modulejsonui.fragment

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.fragment.app.Fragment
import com.example.customview.bean.SegmentDataBean
import com.example.customview.util.LayoutUtils
import com.example.modulejsonui.R


/**
 *
 *
 */
class SonFragment(val contexts: Context,val bean : SegmentDataBean.ListBean) : Fragment() {
    var mContext:Context? = null
    var mBean :SegmentDataBean.ListBean? = null

    var fragment: FrameLayout?=null
    companion object {

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mContext = contexts
        this.mBean = bean
        return inflater.inflate(R.layout.fragment_son, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        try {
            fragment = view.findViewById(R.id.fragment)
            if(mBean?.segmentTitles !=null){
                fragment?.addView(LayoutUtils.addFragmentSonView(mBean!!,contexts!!,this.childFragmentManager))
            }else{
                fragment?.addView(LayoutUtils.addRecyclerView(mContext!!,mBean?.controllers!!))
            }
        }catch (e:Exception){
            e.printStackTrace()
        }


    }

}