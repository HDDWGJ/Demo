package com.example.modulejsonui.adapter;

import android.annotation.SuppressLint;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.List;

public class ViewPagerAdapter extends FragmentPagerAdapter {

    private List<Fragment> list;

    /**
     *
     * @param fm
     * @param list
     * @param notOnResume 是否跟随Acitivity触发Onresume, false是跟随,ture代表自身执行Onresume
     */
    @SuppressLint("WrongConstant")
    public ViewPagerAdapter(FragmentManager fm, List<Fragment> list, boolean notOnResume) {
        super(fm,notOnResume? FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT: FragmentPagerAdapter.BEHAVIOR_SET_USER_VISIBLE_HINT);
        this.list=list;
    }


    @Override
    public Fragment getItem(int position) {
        return list.get(position);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        super.destroyItem(container, position, object);
    }

    @Override
    public long getItemId(int position) {
        return list.get(position).hashCode();
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

}
