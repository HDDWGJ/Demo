package com.example.customview.adapter

import android.content.Context
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.example.customview.util.LayoutUtils

import com.example.customview.util.ViewAddUtils
import com.example.modulejsonui.R
import com.example.modulejsonui.base.BaseRecyclerAdapter
import com.example.modulejsonui.base.RecyclerViewHolder
import com.example.modulejsonui.bean.ButtonBean
import com.example.modulejsonui.bean.ItemViewBean
import com.example.modulejsonui.util.ActivityUtils
import com.example.modulejsonui.util.ItemLayoutUtil


/**
 * Created by Android Studio.
 * User: wsl
 * Date: 7/15/21
 * Time: 11:23 AM
 * Description:
 */
class ListAdapter(private val mContext: Context, var beans: MutableList<ItemViewBean>) : BaseRecyclerAdapter<HashMap<String,String>>(mContext) {

    override fun getLayoutId(viewType: Int): Int {
        return R.layout.item_list_view
    }

    override fun bindView(holder: RecyclerViewHolder?, position: Int) {

        val relatve= holder?.getView<CoordinatorLayout?>(R.id.item)
        var mButton :Button?=null
        var  mBean:ButtonBean ?=null
        relatve?.removeAllViews()
            var mRlLayout = LayoutUtils.setRelativeLayout(context)
            for (bean:ItemViewBean in beans){
                when(bean.type){
                    0 -> {
                        if(bean.labelData.textjsonData !=null){
                            bean.labelData.text = data[position][bean.labelData.textjsonData].toString()
//                            relatve?.addView(ViewAddUtils.addTextView(mContext, bean.labelData))
                            ItemLayoutUtil.RankingItemView(mRlLayout,ViewAddUtils.addTextView(mContext, bean.labelData),bean.labelData.layout)
                        }else{
//                            relatve?.addView(ViewAddUtils.addTextView(mContext, bean.labelData))
                            ItemLayoutUtil.RankingItemView(mRlLayout,ViewAddUtils.addTextView(mContext, bean.labelData),bean.labelData.layout)
                        }

                    }
                    1 -> {
                        mButton = ViewAddUtils.addButtonView(mContext, bean.buttonData)
                        mBean = bean.buttonData
                        relatve?.addView(mButton) //这里赋值还需要改动
//                        ItemLayoutUtil.RankingItemView(mConstraintLayout,mButton,bean.imageData.layout)
                    }
                    2 -> {
//                        if(bean.imageData.textjsonData !=null){
//                            bean.imageData.localImgae = data[position][bean.imageData.textjsonData].toString()
                        relatve?.addView(ViewAddUtils.addImgView(mContext, bean.imageData))
//                        }else{
//                            relatve?.addView(ViewAddUtils.addImgView(mContext, bean.imageData))
//                        }
                    }
                    3 -> {
                        relatve?.addView(ViewAddUtils.addVIew(context, bean.viewData))
                    }
                }
            }


        relatve?.addView(mRlLayout)


        relatve?.setOnClickListener(View.OnClickListener { view ->
//            ViewAddUtils.setOnClick(context, view, "列表 "+position)
            ActivityUtils.openActivity(context,"com.sowell.maoti.ui.personal.wallet.WalletActivity")
        })
        mButton?.setOnClickListener(View.OnClickListener { view ->
            ViewAddUtils.setOnClick(context, view, "按钮 "+data[position],mBean!!.action)
        })



    }


}