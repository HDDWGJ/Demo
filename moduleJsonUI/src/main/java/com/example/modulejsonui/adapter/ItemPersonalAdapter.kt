package com.example.customview.adapter

import android.content.Context
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView

import com.example.customview.bean.TableBean
import com.example.modulejsonui.R
import com.example.modulejsonui.base.BaseRecyclerAdapter
import com.example.modulejsonui.base.RecyclerViewHolder

/**
 * Created by Android Studio.
 * User: wsl
 * Date: 2020/12/11
 * Time: 3:07 PM
 * Description:
 */
class ItemPersonalAdapter(context: Context, lister: ListenerItemBack) : BaseRecyclerAdapter<TableBean>(context) {

    var  mLister : ListenerItemBack = lister

    override fun getLayoutId(viewType: Int): Int {
        return R.layout.item_personal_view
    }


    override fun bindView(holder: RecyclerViewHolder?, position: Int) {
        val relatve= holder?.getView<LinearLayout>(R.id.relatve)
        relatve?.setOnClickListener {
            mLister.ItembackPosition(position)
            refreshData(position)
        }
        val img= holder?.getView<ImageView>(R.id.img)

        val name= holder?.getView<TextView>(R.id.name)
//        name?.setText(data.get(position).listData.title)

    }

    fun refreshData(position: Int){
        notifyDataSetChanged()
    }


    interface ListenerItemBack {
        fun ItembackPosition(position: Int)
    }


}

