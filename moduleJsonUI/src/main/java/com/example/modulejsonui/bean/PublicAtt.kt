package com.example.customview.bean

import java.io.Serializable

/**
 * Created by Android Studio.
 * User: wsl
 * Date: 7/20/21
 * Time: 11:07 AM
 * Description:
 */
data class PublicAtt(var cornerRadius:Int? = 0,
                     var borderColor:String,
                     var borderWidth:Int? = 0,
                     var backgroundColor:String): Serializable
