package com.example.customview.bean

import com.example.modulejsonui.bean.ActionBean
import java.io.Serializable

/**
 * Created by Android Studio.
 * User: wsl
 * Date: 7/16/21
 * Time: 6:05 PM
 * Description:
 */
data class TextBean(var id:Int,
                    var text:String,
                    var fontSize:Int,
                    var textjsonData:String,
                    var numberOfLines:Int,
                    var textColor:String,
                    var weight:String,
                    var aligned:String,
                    var layout: LayoutBean,
                    var action: ActionBean,
                    var publicAtt:PublicAtt): Serializable {


}

