package com.example.customview.bean

import com.example.modulejsonui.bean.NaviDataBean
import com.example.modulejsonui.bean.UrlListBean
import java.io.Serializable

/**
 * Created by Android Studio.
 * User: wsl
 * Date: 7/21/21
 * Time: 3:18 PM
 * Description:
 */
class JsonBean (var type: Int,
                var segmentData: SegmentDataBean?,//多控制器视图
                var listData: NaviDataBean?// item 版面视图
                      ): Serializable