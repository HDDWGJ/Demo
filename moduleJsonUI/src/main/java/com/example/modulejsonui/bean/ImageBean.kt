package com.example.customview.bean

import com.example.modulejsonui.bean.ActionBean
import java.io.Serializable

/**
 * Created by Android Studio.
 * User: wsl
 * Date: 7/16/21
 * Time: 3:31 PM
 * Description:
 */

data class ImageBean(var id:Int,
                     var localImgae:String,
                     var layout:LayoutBean,
                     var textjsonData:String,
                     var action: ActionBean,
                     var publicAtt:PublicAtt): Serializable {

}



