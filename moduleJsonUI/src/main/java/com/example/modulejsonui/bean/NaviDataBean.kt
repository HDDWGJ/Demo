package com.example.modulejsonui.bean

import com.example.customview.bean.ImageBean
import com.example.customview.bean.TableBean
import com.example.customview.bean.TextBean
import com.example.customview.bean.ViewDataBean
import java.io.Serializable

/**
 * Created by Android Studio.
 * User: wsl
 * Date: 7/26/21
 * Time: 7:32 PM
 * Description:
 */
class NaviDataBean(var title:String,
                   var naviData:NaviDataBean,
                   var netList: MutableList<UrlListBean>,// 网络请求地址
                   var items:MutableList<ViewBean>) {
    //顶部导航对象
    data class NaviDataBean(var text:String,
                            var fontSize:Int,
                            var weight:String,
                            var naviData:String,//别名，用来拿取json 数据
                            var backgroundColor:String,
                            var isHideBottomLine:Boolean,
                            var leftView: ItemViewBean,
                            var rightView: ItemViewBean,
                            var centerView:String): Serializable

    data class ViewBean(var type:Int,
                        var jsonData:String,
                        var viewData: ViewDataBean,
                        var imageData: ImageBean,
                        var labelData: TextBean,
                        var buttonData: ButtonBean,
                        var tableData: TableDataBean,///某一条item 的背景，内部还包含子view
                        var listData: TableBean.TitleBean): Serializable
}