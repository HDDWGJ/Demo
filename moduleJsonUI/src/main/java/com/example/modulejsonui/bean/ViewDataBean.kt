package com.example.customview.bean

import com.example.modulejsonui.bean.ItemViewBean

/**
 * Created by Android Studio.
 * User: wsl
 * Date: 7/20/21
 * Time: 11:05 AM
 * Description:
 */
class ViewDataBean(var layout:LayoutBean,
                   var subItem: MutableList<ItemViewBean>,
                   var jsonDataKey:String,//list key
                   var publicAtt:PublicAtt)