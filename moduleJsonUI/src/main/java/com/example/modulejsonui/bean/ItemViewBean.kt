package com.example.modulejsonui.bean

import com.example.customview.bean.ImageBean
import com.example.customview.bean.TableBean
import com.example.customview.bean.TextBean
import com.example.customview.bean.ViewDataBean
import java.io.Serializable

/**
 * Created by Android Studio.
 * User: wsl
 * Date: 7/26/21
 * Time: 7:36 PM
 * Description:
 */

//Item 布局对象
data class ItemViewBean(var type:Int,
                        var jsonDataKey:String,//json 内部的key
                        var imageData: ImageBean,
                        var labelData: TextBean,
                        var viewData: ViewDataBean,
                        var buttonData: ButtonBean,
                        var tableData: TableDataBean,///某一条item 的背景，内部还包含子view
                        var listData: TableBean.TitleBean): Serializable