package com.example.customview.bean


import com.example.modulejsonui.bean.ItemViewBean
import org.json.JSONObject
import java.io.Serializable
import java.util.*


/**
 * Created by Android Studio.
 * User: wsl
 * Date: 7/14/21
 * Time: 3:29 PM
 * Description:
 */
data class  TableBean (var type: Int, var listData: TitleBean):Serializable{

    data class TitleBean(var title:String,
                         var items:MutableList<ItemViewBean>):Serializable


}



