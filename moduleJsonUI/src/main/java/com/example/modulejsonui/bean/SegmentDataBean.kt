package com.example.customview.bean

import com.example.modulejsonui.bean.ItemViewBean

/**
 * Created by Android Studio.
 * User: wsl
 * Date: 7/20/21
 * Time: 4:08 PM
 * Description:整块版面数据模型
 */
    data class SegmentDataBean(var title:String, var isRefresh:Boolean, var headerHeight:Float, var pinSectionHeaderVerticalOffset:Float,
                               var header:MutableList<ItemViewBean>, var segmented:Segmented, var list:MutableList<ListBean>){


        //
        data class Segmented(var titleNormalFont:Float,var titleSelectedFont:Float,var titleNormalWeight:String,var titleSelectedWeight:String,var titleNormalColor:String,
                             var isItemSpacingAverageEnabled:Boolean,var contentEdgeInsetLeft:Float,var contentEdgeInsetRight:Float,var isMaskLine:Boolean,
                             var indicatorHeight:Float,var indicatorWidth:Float,var indicatorWidthIncrement:Float,var indicatorCornerRadius:Float,var indicatorColor:String,
                             var titleSelectedColor:String){}


        //
        data class ListBean(var isDefalut:Boolean, var title:String, var segmented:Segmented, var spacing:Int, var publicAtt:PublicAtt,
                            var segmentTitles:MutableList<String>, var controllers:MutableList<TableBean>)
    }


