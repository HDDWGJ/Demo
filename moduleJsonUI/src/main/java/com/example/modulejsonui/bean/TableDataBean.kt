package com.example.modulejsonui.bean

import com.example.customview.bean.LayoutBean
import com.example.customview.bean.PublicAtt

/**
 * Created by Android Studio.
 * User: wsl
 * Date: 7/30/21
 * Time: 4:23 PM
 * Description:
 */
data class TableDataBean(var layout: LayoutBean,
                         var publicAtt: PublicAtt,
                         var jsonDataKey:String,//list key
                         var items: MutableList<ItemViewBean>
                       )