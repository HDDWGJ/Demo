package com.example.modulejsonui.bean

import com.example.customview.bean.LayoutBean
import com.example.customview.bean.PublicAtt

/**
 * Created by Android Studio.
 * User: wsl
 * Date: 7/30/21
 * Time: 12:05 PM
 * Description:
 */
data class ButtonBean(var text:String,
                      var aligned:String,
                      var textColor:String,
                      var fontSize:Float,
                      var weight:String,
                      var localImgae:String,
                      var imageStyle:String,///// 布局方式,针对图片
                      var spacing:Float,
                      var layout: LayoutBean,
                      var action: ActionBean,
                      var subItem: MutableList<ItemViewBean>,
                      var publicAtt: PublicAtt
)