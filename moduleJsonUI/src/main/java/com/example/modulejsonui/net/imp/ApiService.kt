package com.example.modulejsonui.net.imp

import com.lamfire.json.JSONObject
import com.maoti.lib.net.Constant
import com.maoti.lib.net.ResponseResult
import io.reactivex.Observable
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Path
import java.util.*

/**
 * Created by Android Studio.
 * User: wsl
 * Date: 8/4/21
 * Time: 11:34 AM
 * Description:
 */
interface ApiService {

    @POST(Constant.EditionPath + "/{url}")
    fun getJsonData(@Path(value = "url",encoded = true) user:String, @Body requestBody: RequestBody?): Observable<ResponseResult<JSONObject>?>

}