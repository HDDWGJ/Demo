package com.example.modulejsonui.net

import com.example.modulejsonui.net.imp.ApiService
import com.lamfire.json.JSON
import com.lamfire.json.JSONObject
import com.maoti.lib.net.RetrofitUtil
import com.maoti.lib.net.interceptor.DefaultObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*

/**
 * Created by Android Studio.
 * User: wsl
 * Date: 8/3/21
 * Time: 3:48 PM
 * Description:
 */
class JsonNet : RetrofitUtil() {

    companion object {
        @JvmStatic
        fun getObjectJson(url: String, observer: DefaultObserver<JSONObject>) {
            val json = JSON()
            json.put("display_mode", 0)
            getGsonRetrofit()
                    .create(ApiService::class.java)
                    .getJsonData(url, getJsonBody(json.toString()))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(observer)
        }
    }

}